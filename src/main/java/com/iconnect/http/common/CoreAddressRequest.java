package com.iconnect.http.common;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import org.omg.PortableServer.ServantActivator;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CoreAddressRequest implements Serializable {
    @JsonProperty("firstname")
    @NotNull(message = "firstname may not be null")
    @NotEmpty(message = "firstname may not be empty")
    @Length(min = 3, max = 64)
    String firstName;

    @JsonProperty("lastname")
    @NotNull(message = "lastname may not be null")
    @NotEmpty(message = "lastname may not be empty")
    @Length(min = 3, max = 64)
    String lastName;

    @JsonProperty("phone")
    @NotNull(message = "phone may not be null")
    @NotEmpty(message = "phone may not be empty")
    String phone;

    @JsonProperty("address1")
    @NotNull(message = "address1 may not be null")
    @NotEmpty(message = "address1 may not be empty")
    String address1;

    @JsonProperty("address2")
    String address2;

    String company;

    @JsonProperty("city")
    @NotNull(message = "city may not be null")
    @NotEmpty(message = "city may not be empty")
    String city;

    @JsonProperty("state")
    @NotNull(message = "state may not be null")
    @NotEmpty(message = "state may not be empty")
    String state;

    @JsonProperty("zipcode")
    @NotNull(message = "zipcode may not be null")
    @NotEmpty(message = "zipcode may not be empty")
    String zipcode;

    @JsonProperty("internal_address_id")
    @NotNull(message = "internal_address_id may not be null")
    @NotEmpty(message = "internal_address_id may not be empty")
    String internalAddressID;

}
