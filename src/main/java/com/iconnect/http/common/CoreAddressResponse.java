package com.iconnect.http.common;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.io.Serializable;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CoreAddressResponse implements Serializable {
    String phone;
    String zipcode;
    String firstname;
    String lastname;
    String state;
    String address1;
    String address2;
    String company;
    String city;
    String id;
    String latitude;
    String longitude;
    @JsonIgnore
    String localId;
    @JsonProperty("default")
    String isDefault;
    @JsonProperty("internal_address_id")
    String internalAddressId;

    @JsonIgnore
    public boolean getIsDefault() {
        return Boolean.valueOf(isDefault);
    }

    public void setIsDefault(boolean isDefault) {
        this.isDefault = String.valueOf(isDefault);
    }
}
