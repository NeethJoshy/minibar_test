package com.iconnect.http.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.iconnect.http.response.product.CoreProductResponse;
import com.iconnect.http.response.product.CoreSupplierResponse;
import lombok.Data;

import java.io.Serializable;
import java.util.ArrayList;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CoreProductListResponse  implements Serializable {
    ArrayList<CoreSupplierResponse> suppliers = new ArrayList<>();
    ArrayList<CoreProductResponse> products = new ArrayList<>();
}
