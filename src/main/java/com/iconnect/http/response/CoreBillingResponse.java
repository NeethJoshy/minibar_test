package com.iconnect.http.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.iconnect.http.common.CoreAddressResponse;
import lombok.Data;

import java.io.Serializable;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CoreBillingResponse implements Serializable {
    String id;

    @JsonProperty("cc_card_type")
    String cardType;

    @JsonProperty("cc_last_four")
    String lastFour;

    @JsonProperty("cc_exp_month")
    String expMonth;

    @JsonProperty("cc_exp_yeay")
    String expYear;

    @JsonProperty("default")
    String isDefault;

    public void setIsDefault(boolean isDefault) {
        this.isDefault = String.valueOf(isDefault);
    }

    CoreAddressResponse address;

    @JsonProperty("currency_code")
    String currencyCode;
}
