package com.iconnect.http.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.ArrayList;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CoreBillingListResponse implements Serializable {
    @JsonProperty("payment_profiles")
    ArrayList<CoreBillingResponse> paymentProfiles = new ArrayList<>();
    public void add(CoreBillingResponse coreBillingResponse) {
        paymentProfiles.add(coreBillingResponse);
    }
}
