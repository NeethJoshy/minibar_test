package com.iconnect.http.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CoreBillingTokenResponse {
    @JsonProperty("client_token")
    String clientToken;
}
