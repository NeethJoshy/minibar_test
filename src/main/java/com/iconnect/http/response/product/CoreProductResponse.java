package com.iconnect.http.response.product;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CoreProductResponse implements Serializable {
    String id;
    String brand;
    String name;
    String container_type;
    CorePriceResponse price_range;
    HashMap<String,String> details;
    ArrayList<CoreVarientResponse> variants = new ArrayList<>();

    public void addVariant(CoreVarientResponse coreVarientResponse){
        variants.add(coreVarientResponse);
    }

}
