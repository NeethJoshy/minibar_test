package com.iconnect.http.response.product;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.io.Serializable;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CoreDeliveryMethodResponse  implements Serializable {
    private String delivery_minimum;

    @JsonProperty("allows_tipping")
    private String allowsTipping;

    public void setAllowsTipping(String allowsTipping) {
        this.allowsTipping = allowsTipping;
    }

    private String supplier_id;

    private CoreHoursResponse hours;

    private String delivery_expectation;

    private String free_delivery_threshold;

    private String per_item_delivery_fee;

    private CoreNextSchedulingWindowResponse next_scheduling_window;

    private String type;

    @JsonProperty("default")
    private String isDefault;

    public void setIsDefault(boolean isDefault) {
        this.isDefault = String.valueOf(isDefault);
    }

    private String next_delivery;

    private String name;

    private String scheduling_mode;

    private String delivery_fee;

    @JsonProperty("allows_scheduling")
    private String allowsScheduling;

    public void setAllowsScheduling(String allowsScheduling) {
        this.allowsScheduling = allowsScheduling;
    }

    private String maximum_delivery_expectation;
}
