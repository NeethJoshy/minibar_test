package com.iconnect.http.response.product;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.io.Serializable;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CoreVarientResponse implements Serializable {
    String price;
    String volume;
    @JsonProperty("item_id")
    String itemId;
    @JsonProperty("supplier_id")
    String supplierId;
    @JsonProperty("pack_size")
    String packSize;
    @JsonProperty("quantity_on_hand")
    String quantityOnHand;
    @JsonProperty("delivery_minimum")
    String deliveryMinimum;

}
