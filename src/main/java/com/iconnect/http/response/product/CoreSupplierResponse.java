package com.iconnect.http.response.product;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.iconnect.http.common.CoreAddressResponse;
import lombok.Data;

import java.io.Serializable;
import java.util.ArrayList;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CoreSupplierResponse implements Serializable {

    String id;
    String name;
    String type;
    CoreAddressResponse address;
    String distance;
    @JsonProperty("delivery_methods")
    ArrayList<CoreDeliveryMethodResponse> deliveryMethods;
    String time_zone;
    String best_delivery_minimum;
    String best_delivery_fee;
    String best_delivery_estimate;
    String opens_at;
    String closes_at;

}
