package com.iconnect.http.response.product;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.io.Serializable;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CorePriceResponse implements Serializable {
    String min;
    String max;
    String container_type;
}
