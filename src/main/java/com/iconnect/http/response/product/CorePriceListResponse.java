package com.iconnect.http.response.product;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.util.ArrayList;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CorePriceListResponse {
    ArrayList<CorePriceResponse> prices;

}
