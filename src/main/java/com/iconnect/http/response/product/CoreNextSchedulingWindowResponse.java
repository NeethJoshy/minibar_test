package com.iconnect.http.response.product;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.io.Serializable;
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CoreNextSchedulingWindowResponse implements Serializable {
    private String end_time;

    private String start_time;
}
