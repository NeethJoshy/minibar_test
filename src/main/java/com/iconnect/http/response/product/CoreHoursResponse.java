package com.iconnect.http.response.product;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.io.Serializable;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CoreHoursResponse implements Serializable {
    private String always_open;

    private String closes_at;

    private String opens_at;
}
