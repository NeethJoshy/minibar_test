package com.iconnect.http.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.iconnect.http.common.CoreAddressResponse;
import lombok.Data;

import java.util.ArrayList;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CoreAddressListResponse {
    ArrayList<CoreAddressResponse> address = new ArrayList<>();

    public void add(CoreAddressResponse coreAddressResponse) {
        address.add(coreAddressResponse);
    }
}
