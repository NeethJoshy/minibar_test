package com.iconnect.http.response.order;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.iconnect.http.common.CoreAddressResponse;
import com.iconnect.http.response.CoreBillingResponse;
import lombok.Data;

import java.io.Serializable;
import java.util.ArrayList;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CoreOrderResponse implements Serializable {
    @JsonProperty("user_id")
    private String userID;

    private String number;

    private String state;

    private ArrayList<CoreAmountsResponse> amounts = new ArrayList<>();

    public void add(CoreAmountsResponse coreAmountsResponse) {
        this.amounts.add(coreAmountsResponse);
    }

    private ArrayList<CoreDeliveryTimeResponse> delivery_times = new ArrayList<>();

    public void add(CoreDeliveryTimeResponse coreDeliveryTimeResponse) {
        this.delivery_times.add(coreDeliveryTimeResponse);
    }

    @JsonProperty("display_state")
    private String displayState;

    @JsonProperty("order_items")
    private ArrayList<CoreResponseOrderItems> orderItems = new ArrayList<>();

    public void add(CoreResponseOrderItems coreResponseOrderItems) {
        orderItems.add(coreResponseOrderItems);
    }

    @JsonProperty("payment_profile")
    private CoreBillingResponse paymentProfile;

    @JsonProperty("shipping_address")
    private CoreAddressResponse shippingAddress;

    @JsonProperty("delivery_notes")
    private String deliveryNotes;

    @JsonProperty("promo_code")
    private String promoCode;

    @JsonProperty("qualified_deals")
    private ArrayList<String> qualifiedDeals;

    //finalized
    @JsonProperty("completed_at")
    private String completed_at;
}
