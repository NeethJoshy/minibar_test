package com.iconnect.http.response.order;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class CoreAvailableDeliveryWindow {
    @JsonProperty("label")
    String Label;
    @JsonProperty("value")
    String Value;
}
