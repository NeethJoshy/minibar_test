package com.iconnect.http.response.order;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.io.Serializable;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CoreDiscountsResponse implements Serializable {
    private String coupons = "0";

    private String deals = "0";
}
