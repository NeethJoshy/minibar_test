package com.iconnect.http.response.order;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CoreAmountsResponse {
    private String total;

    private String shipping;

    private String tax;

    private CoreDiscountsResponse discounts = new CoreDiscountsResponse();

    private String subtotal;

    private String coupon;

    private String tip;

    @JsonProperty("supplier_id")
    private String supplierID;
}
