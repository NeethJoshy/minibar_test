package com.iconnect.http.response.order;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.iconnect.http.response.product.CoreSupplierResponse;
import com.iconnect.rowjson.RawObject;
import lombok.Data;

import java.io.Serializable;
import java.util.ArrayList;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CoreOrderResponseContainer implements Serializable {
    String vendor;
    ArrayList<CoreSupplierResponse> suppliers;
    CoreOrderResponse order;
    @JsonProperty("available_delivery_windows")
    ArrayList<CoreAvailableDeliveryWindow> availableDeliveryWindows;

    @JsonProperty("tracking")
    CoreTrackingResponse tracking;

    RawObject vendorResponse;
}
