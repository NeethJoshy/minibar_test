package com.iconnect.http.response.order;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.io.Serializable;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CoreResponseOrderItems implements Serializable {
    private String total;

    private String id;

    private String supplier_id;

    private String price;

    private String product_id;

    private String name;

    private String volume;

    private String quantity;

    private String scheduled_for;

    private String delivery_method_id;
}
