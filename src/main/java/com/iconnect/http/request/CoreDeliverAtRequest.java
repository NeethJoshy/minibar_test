package com.iconnect.http.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class CoreDeliverAtRequest {
    @JsonProperty("deliver_at")
    String deliverAt;

    @JsonProperty("coupen_code")
    String coupenCode;
}
