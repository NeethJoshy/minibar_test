package com.iconnect.http.request;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.iconnect.http.common.CoreAddressRequest;
import lombok.Data;


import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CoreBillingRequest implements Serializable {
    @NotNull(message = "address may not be null")
    CoreAddressRequest address;

    @NotNull(message = "payment_method_nonce may not be null")
    @NotEmpty(message = "payment_method_nonce may not be empty")
    @JsonProperty("payment_method_nonce")
    String paymentMethodNonce;
}
