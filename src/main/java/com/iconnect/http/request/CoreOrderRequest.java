package com.iconnect.http.request;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.ArrayList;


@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CoreOrderRequest implements Serializable {
    @NotNull(message = "payment_id may not be null")
    @NotEmpty(message = "payment_id may not be empty")
    @JsonProperty("payment_id")
    private String paymentId;

    @NotNull(message = "shipping_id may not be null")
    @NotEmpty(message = "shipping_id may not be empty")
    @JsonProperty("shipping_id")
    private String shippingId;

    @NotNull(message = "order_items may not be null")
    @NotEmpty(message = "order_items may not be empty")
    @JsonProperty("order_items")
    @Valid
    ArrayList<CoreOrderItems> orderItems;

    private String tip = "0";

    @JsonProperty("deliver_at")
    //@NotNull(message = "deliver_at may not be null")
    //@NotEmpty(message = "deliver_at may not be empty")
    private String deliverAt;

    @JsonProperty("delivery_location")
    CoreLocationRequest location;

    @JsonProperty("delivery_note")
    private String deliveryNote;

    @JsonProperty("coupon_code")
    private String couponCode;
}
