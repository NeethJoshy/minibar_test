package com.iconnect.http.request;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
//import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CoreAddressRequestProductList extends CoreLocationRequest implements Serializable {
    String phone;

    @NotNull(message = "zipcode may not be null")
    @NotEmpty(message = "zipcode may not be empty")
    String zipcode;

    String name;

    String state;

    String address1;

    String address2;
    String company;

    String city;

    @JsonProperty("default")
    String isDefault;

    public boolean getIsDefault() {
        return Boolean.valueOf(isDefault);
    }
}
