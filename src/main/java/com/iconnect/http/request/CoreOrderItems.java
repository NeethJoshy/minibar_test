package com.iconnect.http.request;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;


import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CoreOrderItems implements Serializable {
    public CoreOrderItems(String vendor_product_id, String quantity) {
        this.vendor_product_id = vendor_product_id;
        this.quantity = quantity;
    }

    public CoreOrderItems() {

    }

    @NotNull(message = "product_id may not be null")
    @NotEmpty(message = "product_id may not be empty")
    @JsonProperty("product_id")
    private String product_id;

    @NotNull(message = "vendor_product_id may not be null")
    @NotEmpty(message = "vendor_product_id may not be empty")
    @JsonProperty("vendor_product_id")
    private String vendor_product_id;

    @NotNull(message = "quantity may not be null")
    @NotEmpty(message = "quantity may not be empty")
    private String quantity;
}
