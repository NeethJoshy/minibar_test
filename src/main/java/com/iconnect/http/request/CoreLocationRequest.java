package com.iconnect.http.request;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CoreLocationRequest {
    @NotNull(message = "latitude may not be null")
   // @NotEmpty(message = "latitude may not be empty")
    String latitude;

    @NotNull(message = "longitude may not be null")
   // @NotEmpty(message = "longitude may not be empty")
    String longitude;
}
