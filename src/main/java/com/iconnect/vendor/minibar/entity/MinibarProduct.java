package com.iconnect.vendor.minibar.entity;

import lombok.Data;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.ParamDef;

import javax.persistence.*;

@Entity(name = "minibar_product")
@Data
@FilterDef(name = "tenantFilter", parameters = {@ParamDef(name = "tenant_id", type = "string")})
@Filter(name = "tenantFilter", condition = "tenant_id = :tenant_id")
public class MinibarProduct extends Tenant {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "product_id")
    private String productID;

    @Column(name = "brand_name")
    private String minibarBrandName;

    @Column(name = "brand_key")
    private String minibarBrandKey;

    @Column(name = "product_grouping_id")
    private String productGroupingID;

    @Column(name = "product_filter_name")
    private String productFilterName;


    @Column(name = "product_name")
    private String productName;

}
