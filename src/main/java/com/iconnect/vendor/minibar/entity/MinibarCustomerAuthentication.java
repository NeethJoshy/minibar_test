package com.iconnect.vendor.minibar.entity;


import lombok.Data;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.ParamDef;

import javax.persistence.*;
import java.io.Serializable;


@Entity(name = "minibar_customer_auth")
@Data
@FilterDef(name = "identifierFilter", parameters = {@ParamDef(name = "identifier_id", type = "string")})
@Filter(name = "identifierFilter", condition = "identifier_id = :identifier_id")
public class MinibarCustomerAuthentication extends Identifier implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "customer_id")
    private String customerid;

   // @Convert(converter = StringCryptoConverter.class)
    private String username;

   // @Convert(converter = StringCryptoConverter.class)
    private String password;

    @Column(name = "user_id")
    private String userID;

    @Column(name = "successfully_registered")
    private Integer successfullyRegistered = 0;

    @Column(name = "registration_error")
    private String registrationError;

    @Column(name = "new_user")
    private Integer newUser = 0;

    public boolean hasSuccessfullyRegistered() {
        return successfullyRegistered > 0;
    }
}
