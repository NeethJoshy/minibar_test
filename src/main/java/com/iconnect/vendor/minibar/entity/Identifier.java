package com.iconnect.vendor.minibar.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.iconnect.vendor.minibar.Configurations.Constants;
import lombok.AccessLevel;
import lombok.Data;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.OffsetDateTime;

@Data
@MappedSuperclass
public class Identifier implements Serializable {
    @Column(name = Constants.TABLE_COLUMN_IDENTIFIER, nullable = false)
    @NotNull
    protected String identifier_id;

    @Column(name = "created_at", updatable = false)
    @JsonProperty(value = "created_at")
    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ssX")
    @CreationTimestamp
    @Setter(value = AccessLevel.NONE)
    private OffsetDateTime createdAt;

    @Column(name = "updated_at",insertable = false)
    @JsonProperty(value = "updated_at")
    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ssX")
    @UpdateTimestamp
    @Setter(value = AccessLevel.NONE)
    private OffsetDateTime updatedAt;
}
