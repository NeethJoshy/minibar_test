package com.iconnect.vendor.minibar.entity;

import lombok.Data;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.ParamDef;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity(name = "minibar_external_authentication")
@Data
@FilterDef(name = "tenantFilter", parameters = {@ParamDef(name = "tenant_id", type = "string")})
@Filter(name = "tenantFilter", condition = "tenant_id = :tenant_id")
public class MinibarExternalAuthentication extends Tenant {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String client_id;
    private String client_secret;
    private String global_token;

}
