package com.iconnect.vendor.minibar.entity;

import lombok.Data;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.ParamDef;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity(name = "minibar_order")
@Data
@FilterDef(name = "tenantFilter", parameters = {@ParamDef(name = "tenant_id", type = "string")})
@Filter(name = "tenantFilter", condition = "tenant_id = :tenant_id")
public class MinibarOrder extends Tenant {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "customer_id", nullable = false)
    @NotNull
    private String customerID;

    @Column(name = "vendor_order_number", nullable = false)
    @NotNull
    String vendorOrderNumber;

    @Column(name = "order_request", nullable = false)
    @NotNull
    String orderRequest;

    @Column(name = "vendor_order_response")
    String vendorOrderResponse;

    @Column(name = "vendor_order_finalized_response")
    String vendorOrderFinalizedResponse;

    @Column(name = "has_finalized")
    Integer hasFinalized = 0;

    @Column(name = "order_submitted")
    Integer orderSubmitted = 0;

    public boolean isFinalized() {
        return hasFinalized > 0;
    }

    public boolean isSubmitted() {return orderSubmitted > 0 ;}
}
