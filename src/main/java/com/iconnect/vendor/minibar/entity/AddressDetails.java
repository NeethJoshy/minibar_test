package com.iconnect.vendor.minibar.entity;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.ParamDef;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity(name = "minibar_customer_address_details")
@Data
@FilterDef(name = "identifierFilter", parameters = {@ParamDef(name = "identifier_id", type = "string")})
@Filter(name = "identifierFilter", condition = "identifier_id = :identifier_id")

public class AddressDetails extends Identifier {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "customer_id" , nullable = false)
    @NotNull
    private String customerID;

    @Column(name = "internal_address_id", nullable = false)
    @NotNull
    private String internalAddressID;

    @Column(name = "vendor_address_id", nullable = false)
    @NotNull
    private String vendorAddressID;

    @Column(name = "firstname", nullable = false)
    @NotNull
    private String firstName;

    @Column(name = "lastname", nullable = false)
    @NotNull
    private String lastName;

    @Column(name = "address1", nullable = false)
    @NotNull
    private String address1;

    @Column(name = "address2")
    private String address2;

    @Column(name = "company")
    private String company;

    @Column(name = "city", nullable = false)
    @NotNull
    private String city;

    @Column(name = "state", nullable = false)
    @NotNull
    private String state;

    @Column(name = "zipcode", nullable = false)
    @NotNull
    private String zipcode;

    @Column(name = "phone", nullable = false)
    @NotNull
    private String phone;

    @Column(name = "notes")
    private String notes;

    @Column(name = "country")
    private String country;

    @Column(name = "latitude")
    private String latitude;

    @Column(name = "longitude")
    private String longitude;

    @Column(name = "is_default")
    private int isDefault = 0;


}
