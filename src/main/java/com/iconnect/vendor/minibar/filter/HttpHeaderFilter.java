package com.iconnect.vendor.minibar.filter;

import com.iconnect.vendor.minibar.Configurations.Constants;
import com.iconnect.vendor.minibar.core.CurrentRequestScope;
import org.slf4j.LoggerFactory;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
@Order(5)
public class HttpHeaderFilter implements Filter {
    private static final org.slf4j.Logger log = LoggerFactory.getLogger(HttpHeaderFilter.class);
    @Override
    public void init(FilterConfig filterConfig) {
        //have to implement this method as the class is abstract
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        String identifierHeader = request.getHeader(Constants.HTTP_IDENTIFIER_HEADER);
        String tenantHeader = request.getHeader(Constants.HTTP_TENANT_HEADER);
        String customerHeader = request.getHeader(Constants.HTTP_CUSTOMER_HEADER);
        CurrentRequestScope.setCustomerID(customerHeader);
        log.info("{}",tenantHeader);
        if (tenantHeader != null) {

            CurrentRequestScope.setCurrentTenant(tenantHeader);
            CurrentRequestScope.setCurrentIdentifier(identifierHeader);
            filterChain.doFilter(servletRequest, servletResponse);
            return;
        }
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
    }

    @Override
    public void destroy() {
        //have to implement this method as the class is abstract
    }
}
