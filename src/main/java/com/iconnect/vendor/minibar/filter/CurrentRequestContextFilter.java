package com.iconnect.vendor.minibar.filter;

import com.iconnect.vendor.minibar.core.CurrentRequestScope;
import com.iconnect.vendor.minibar.entity.MinibarExternalAuthentication;
import com.iconnect.vendor.minibar.services.business.MinibarProductProxy;
import com.iconnect.vendor.minibar.services.database.tenant.MinibarExternalAuthenticationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
@Order(6)
public class CurrentRequestContextFilter implements Filter {
    private static final Logger log = LoggerFactory.getLogger(CurrentRequestContextFilter.class);

    @Autowired
    MinibarExternalAuthenticationService minibarExternalAuthenticationService;

//    @Autowired
//    MinibarCustomerAuthenticationService minibarCustomerAuthenticationService;

    @Autowired
    MinibarProductProxy minibarProductProxy;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        //have to implement this method as the class is abstract
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        do {

            String tenant = CurrentRequestScope.getCurrentTenant();
            MinibarExternalAuthentication mea = minibarExternalAuthenticationService.getTenantExternalAuthenticationDetails();
            if (mea == null) {
                log.error("No External Authentication for minibar available for Company '{}' Tenant '{}' ", CurrentRequestScope.getCurrentIdentifier(), tenant);
                break;
            }
//            String customer = CurrentRequestScope.getCustomerID();
//            if (customer != null) {
//                MinibarCustomerAuthentication mca = minibarCustomerAuthenticationService.getCustomerDetails(customer);
//                if (mca == null) {
//                    log.error("No Customer Authentication for minibar available for Customer '{}' Company '{}' Tenant '{}' ", customer, CurrentRequestScope.getCurrentIdentifier(), tenant);
//                    break;
//                }
//            }

            CurrentRequestScope.setProductModel(minibarProductProxy.fetchProductModel());
            filterChain.doFilter(servletRequest, servletResponse);
            return;
        } while (false);
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
    }

    @Override
    public void destroy() {
        //have to implement this method as the class is abstract
    }

}
