package com.iconnect.vendor.minibar.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.io.Serializable;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class PermalinkBrand implements Serializable {
    String brandKey;
    String productID;
}
