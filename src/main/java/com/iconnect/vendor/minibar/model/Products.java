package com.iconnect.vendor.minibar.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.iconnect.vendor.minibar.entity.MinibarProduct;
import lombok.Data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;


@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Products implements Serializable {
    private HashMap<String, ArrayList<String>> brandProductGroupings = new HashMap<>();
    private HashSet<String> brands = new HashSet<>();
    private HashMap<String, PermalinkBrand> productID = new HashMap<>();
    private HashMap<String, String> brandkeyName = new HashMap<>();
    private ArrayList<MinibarProduct> minibarProducts;
    private HashMap<String, ArrayList<String>> brandProductFilter = new HashMap<>();

    public void add(ArrayList<MinibarProduct> minibarProductList) {
        minibarProducts = minibarProductList;
        for (MinibarProduct minibarProduct : minibarProductList) {
            String currentBrand = minibarProduct.getMinibarBrandKey();
            brands.add(currentBrand);
            brandkeyName.putIfAbsent(minibarProduct.getMinibarBrandKey(), minibarProduct.getMinibarBrandName());
            if (!brandProductGroupings.containsKey(currentBrand)) {
                brandProductGroupings.put(currentBrand, new ArrayList<>());
            }
            if (!brandProductFilter.containsKey(currentBrand)) {
                brandProductFilter.put(currentBrand, new ArrayList<>());
            }
            brandProductGroupings.get(currentBrand).add(minibarProduct.getProductGroupingID());
            brandProductFilter.get(currentBrand).add((minibarProduct.getProductFilterName()));
            PermalinkBrand permalinkBrand = new PermalinkBrand();
            permalinkBrand.setBrandKey(currentBrand);
            permalinkBrand.setProductID(minibarProduct.getProductID().toLowerCase());
            productID.put(minibarProduct.getProductFilterName().toLowerCase(), permalinkBrand);
        }
    }

    @JsonIgnore
    public ArrayList<String> getBrandProductGrouping() {
        ArrayList<String> productGroupings = new ArrayList<>();

        Set<String> keys = brandProductGroupings.keySet();
        for (String key : keys) {
            productGroupings.addAll(brandProductGroupings.get(key));
        }
        return productGroupings;
    }
    @JsonIgnore
    public String getCurrentBrands() {
        return String.join(",", brands.stream().collect(Collectors.toList()));
    }

    @JsonIgnore
    public PermalinkBrand getProductID(String productPermalink) {
        return productID.getOrDefault(productPermalink, null);
    }

    @JsonIgnore
    public String getBrandName(String brandKey) {
        return brandkeyName.getOrDefault(brandKey, "");
    }

    @JsonIgnore
    public String getPermalinkId(String productId){
        for(MinibarProduct minibarProduct:minibarProducts){
            if(minibarProduct.getProductID().equalsIgnoreCase(productId)){
                return minibarProduct.getProductGroupingID();
            }
        }
        return null;
    }
}
