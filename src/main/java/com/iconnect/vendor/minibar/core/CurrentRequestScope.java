package com.iconnect.vendor.minibar.core;

import com.iconnect.vendor.minibar.model.ProductModel;

public class CurrentRequestScope {
    private static ThreadLocal<String> tenantID = new ThreadLocal<>();
    private static ThreadLocal<String> identifierID = new ThreadLocal<>();
    private static ThreadLocal<ProductModel> productModel = new ThreadLocal<>();
    private static ThreadLocal<String> accessToken = new ThreadLocal<>();
    private static ThreadLocal<String> customerID = new ThreadLocal<>();
    private static ThreadLocal<String> userToken = new ThreadLocal<>();

    public static String getCurrentTenant() {
        return tenantID.get();
    }
    public static void setCurrentTenant(String tenantid) {
        tenantID.set(tenantid);
    }

    public static String getCurrentIdentifier() {
        return identifierID.get();
    }

    public static void setCurrentIdentifier(String identifier) {
        identifierID.set(identifier);
    }

    public static void setAccessToken(String access_token) {
        accessToken.set(access_token);
    }

    public static ProductModel getProductModel() {
        return productModel.get();
    }

    public static String getBearerToken() {
        return "Bearer " + accessToken.get();
    }

    public static void setCustomerID(String customer_id) {
        customerID.set(customer_id);
    }
    public static String getCustomerID() {
        return customerID.get();
    }

    public static void setProductModel(ProductModel productModel) {
        CurrentRequestScope.productModel.set(productModel);
    }

    public static void setUserToken(String user_token) {
        userToken.set(user_token);
    }

    public static String getUserToken() {
        return "Bearer " + userToken.get();
    }


}
