package com.iconnect.vendor.minibar.core;

import org.springframework.cache.CacheManager;
import org.springframework.cache.interceptor.CacheOperationInvocationContext;
import org.springframework.cache.interceptor.SimpleCacheResolver;
import org.springframework.core.env.PropertyResolver;

import java.util.Collection;
import java.util.Collections;
import java.util.stream.Collectors;

public class PropertyResolvingCacheResolverCompany extends SimpleCacheResolver {
    private final PropertyResolver propertyResolver;

    public PropertyResolvingCacheResolverCompany(CacheManager cacheManager, PropertyResolver propertyResolver) {
        super(cacheManager);
        this.propertyResolver = propertyResolver;
    }

    @Override
    protected Collection<String> getCacheNames(CacheOperationInvocationContext<?> context) {
        String currentIdentifier = CurrentRequestScope.getCurrentIdentifier();

        Collection<String> unresolvedCacheNames = super.getCacheNames(context);

        Collection<String> previousCollection = unresolvedCacheNames.stream()
                .map(unresolvedCacheName -> propertyResolver.resolveRequiredPlaceholders(unresolvedCacheName)).collect(Collectors.toList());

        return Collections.singletonList(currentIdentifier + ":" + previousCollection.iterator().next());
    }
}
