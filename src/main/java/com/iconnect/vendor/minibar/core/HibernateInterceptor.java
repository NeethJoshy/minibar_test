package com.iconnect.vendor.minibar.core;

import com.iconnect.vendor.minibar.Configurations.Constants;
import com.iconnect.vendor.minibar.entity.Identifier;
import com.iconnect.vendor.minibar.entity.Tenant;
import org.hibernate.EmptyInterceptor;

import java.io.Serializable;

public class HibernateInterceptor extends EmptyInterceptor {

    @Override
    public void onDelete(Object entity, Serializable id, Object[] state, String[] propertyNames, org.hibernate.type.Type[] types) {
        if (!((entity instanceof Tenant) || (entity instanceof Identifier))) {
            return;
        }
        modifyObject(state, propertyNames);
    }

    @Override
    public boolean onFlushDirty(Object entity, Serializable id, Object[] currentState, Object[] previousState, String[] propertyNames, org.hibernate.type.Type[] types) {
        if (!((entity instanceof Tenant) || (entity instanceof Identifier))) {
            return false;
        }
        modifyObject(currentState, propertyNames);
        return true;
    }

    @Override
    public boolean onSave(Object entity, Serializable id, Object[] state, String[] propertyNames, org.hibernate.type.Type[] types) {
        if (!((entity instanceof Tenant) || (entity instanceof Identifier))) {
            return false;
        }
        modifyObject(state, propertyNames);
        return true;
    }

    private void modifyObject(Object[] state, String[] propertyNames) {
        final int length = state.length;
        for (int i = 0; i < length; i++) {
            if (propertyNames[i].equals(Constants.TABLE_COLUMN_TENANT)) {
                state[i] = CurrentRequestScope.getCurrentTenant();
            }
            if (propertyNames[i].equals(Constants.TABLE_COLUMN_IDENTIFIER)) {
                state[i] = CurrentRequestScope.getCurrentIdentifier();
            }
        }
    }
}
