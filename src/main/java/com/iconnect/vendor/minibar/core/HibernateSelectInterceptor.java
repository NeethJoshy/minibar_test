package com.iconnect.vendor.minibar.core;

import com.iconnect.vendor.minibar.Configurations.Constants;
import com.iconnect.vendor.minibar.services.database.company.EntityManagerServiceCompany;
import com.iconnect.vendor.minibar.services.database.tenant.EntityManagerServiceTenant;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.hibernate.Session;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class HibernateSelectInterceptor  {
    @Before("execution(* com.iconnect.vendor.minibar.services.database..* (..)) && target(entityManagerServiceTenant)")
    public void aroundMultiTenantServiceExecution(JoinPoint pjp, EntityManagerServiceTenant entityManagerServiceTenant) {
        System.out.println("ghfghg");
        org.hibernate.Filter filterTenant = entityManagerServiceTenant.entityManager.unwrap(Session.class).enableFilter("tenantFilter");
        filterTenant.setParameter(Constants.TABLE_COLUMN_TENANT, CurrentRequestScope.getCurrentTenant());
        filterTenant.validate();

    }

    @Before("execution(* com.iconnect.vendor.minibar.services.database..* (..)) && target(entityManagerServiceCompany)")
    public void aroundComapntServiceExecution(JoinPoint pjp, EntityManagerServiceCompany entityManagerServiceCompany) {
        org.hibernate.Filter filterIdentifier = entityManagerServiceCompany.entityManager.unwrap(Session.class).enableFilter("identifierFilter");
        filterIdentifier.setParameter(Constants.TABLE_COLUMN_IDENTIFIER, CurrentRequestScope.getCurrentIdentifier());
        filterIdentifier.validate();
    }
}
