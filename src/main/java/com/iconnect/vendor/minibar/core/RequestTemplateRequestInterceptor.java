package com.iconnect.vendor.minibar.core;

import com.iconnect.vendor.minibar.Configurations.Constants;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpRequest;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;

import java.io.IOException;

public class RequestTemplateRequestInterceptor implements ClientHttpRequestInterceptor {
    @Override
    public ClientHttpResponse intercept(HttpRequest httpRequest, byte[] body, ClientHttpRequestExecution clientHttpRequestExecution) throws IOException {
        HttpHeaders headers = httpRequest.getHeaders();
        headers.add(Constants.HTTP_TENANT_HEADER, CurrentRequestScope.getCurrentTenant());
        headers.add(Constants.HTTP_IDENTIFIER_HEADER, CurrentRequestScope.getCurrentIdentifier());

        if (CurrentRequestScope.getCustomerID() != null) {
            headers.add(Constants.HTTP_CUSTOMER_HEADER, CurrentRequestScope.getCustomerID());
        }
        return clientHttpRequestExecution.execute(httpRequest, body);
    }
}
