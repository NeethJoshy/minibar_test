package com.iconnect.vendor.minibar.services.business;

import com.iconnect.http.response.product.CorePriceResponse;
import com.iconnect.vendor.minibar.core.CustomObjectMapper;
import com.iconnect.vendor.minibar.entity.MinibarExternalAuthentication;
import com.iconnect.vendor.minibar.minibar.common.Address;
import com.iconnect.vendor.minibar.minibar.request.*;
import com.iconnect.vendor.minibar.minibar.response.*;
import com.iconnect.vendor.minibar.minibar.response.order.ResponseOrder;
import com.iconnect.vendor.minibar.minibar.response.order.ResponseOrderFinalized;
import com.iconnect.vendor.minibar.minibar.response.product.ProductList;
import com.iconnect.vendor.minibar.minibar.response.product.Product_grouping;
import com.iconnect.vendor.minibar.minibar.response.supplier.ResponseSupplierList;
import com.iconnect.vendor.minibar.services.http.MinibarHttpService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;


import java.io.IOException;

@Service
public class MinibarHttpProxy  {

    @Autowired
    MinibarHttpService minibarHttpService;

    CustomObjectMapper objectMapper = new CustomObjectMapper();

    public AccessToken fetchAccessToken(MinibarExternalAuthentication mea)  throws IOException{
        ResponseEntity<String> response = minibarHttpService.fetchAccessToken(mea);
        System.out.println(response);
        return objectMapper.readValue(response.getBody(), AccessToken.class);
    }
    public ResponseSupplierList getSuppliers(SuppliersParam suppliersParam) throws IOException {
        ResponseEntity<String> response = minibarHttpService.getSuppliers(suppliersParam);
        return objectMapper.readValue(response.getBody(), ResponseSupplierList.class);
    }
    public ProductList getProducts(SuppliersList suppliersList) throws IOException {
        ResponseEntity<String> response = minibarHttpService.getProducts(suppliersList);
        return objectMapper.readValue(response.getBody(), ProductList.class);
    }


    public Product_grouping getProducts(String permalink) throws IOException{
        ResponseEntity<String> response = minibarHttpService.getProducts(permalink);
        return objectMapper.readValue(response.getBody(),Product_grouping.class);
    }

    public OauthToken authenticateUserOauth(CreateUser signinUser) throws IOException {
        ResponseEntity<String> response = minibarHttpService.authenticateUserOauth(signinUser);
        return objectMapper.readValue(response.getBody(), OauthToken.class);
    }

    public AuthenticatedUser fetchUser() throws IOException{
        ResponseEntity<String> response = minibarHttpService.fetchUser();
        return objectMapper.readValue(response.getBody(),AuthenticatedUser.class);
    }

    public Address createAddress(Address address) throws  IOException{
        ResponseEntity<String> response = minibarHttpService.createAddress(address);
        return objectMapper.readValue(response.getBody(),Address.class);
    }
    public SuccessResponse deleteAddress(String addressID) throws IOException{
        ResponseEntity<String> response = minibarHttpService.deleteAddress(addressID);
        return  objectMapper.readValue(response.getBody(),SuccessResponse.class);
    }

    public CreditCard createCreditCard(CreateCreditCard createCreditCard) throws IOException {
        ResponseEntity<String> response = minibarHttpService.createCreditCard(createCreditCard);
        return objectMapper.readValue(response.getBody(), CreditCard.class);
    }

    public SuccessResponse deleteCreditCard(String creditCardID) throws IOException {
        ResponseEntity<String> response = minibarHttpService.deleteCreditCard(creditCardID);
        return objectMapper.readValue(response.getBody(), SuccessResponse.class);
    }

    public BraintreeToken getBraintreeSDKToken() throws IOException {
        ResponseEntity<String> response = minibarHttpService.getBraintreeSDKToken();
        return objectMapper.readValue(response.getBody(), BraintreeToken.class);
    }

    public ResponseOrder createOrder(Order order) throws IOException {
        ResponseEntity<String> response = minibarHttpService.createOrder(order);
        return objectMapper.readValue(response.getBody(), ResponseOrder.class);
    }

    public ResponseOrderFinalized finalizeOrder(String orderNumber) throws IOException{
        ResponseEntity<String> response = minibarHttpService.finalizeOrder(orderNumber);
        return objectMapper.readValue(response.getBody(),ResponseOrderFinalized.class);
    }

}
