package com.iconnect.vendor.minibar.services.transformers;

import com.iconnect.http.response.order.*;
import com.iconnect.vendor.minibar.Configurations.Constants;
import com.iconnect.vendor.minibar.minibar.response.order.Amounts;
import com.iconnect.vendor.minibar.minibar.response.order.Discounts;
import com.iconnect.vendor.minibar.minibar.response.order.ResponseOrder;
import com.iconnect.vendor.minibar.minibar.response.order.ResponseOrderItems;

import java.time.OffsetDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;

public class OrderToResponse {
    public static CoreOrderResponseContainer transform(ResponseOrder responseOrder) {
        CoreOrderResponseContainer coreOrderResponseContainer = createCoreOrderResponseContainer();
        coreOrderResponseContainer.setOrder(transformOrder(responseOrder));
        return coreOrderResponseContainer;
    }

    private static CoreOrderResponseContainer createCoreOrderResponseContainer() {
        CoreOrderResponseContainer coreOrderResponseContainer = new CoreOrderResponseContainer();
        coreOrderResponseContainer.setVendor(Constants.VENDOR);
        return coreOrderResponseContainer;
    }


    public static CoreOrderResponse transformOrder(ResponseOrder responseOrder) {
        CoreOrderResponse coreOrderResponse = new CoreOrderResponse();
        coreOrderResponse.setShippingAddress(AddressToResponse.transform(responseOrder.getShipping_address()));
        coreOrderResponse.setPaymentProfile(CreditCardToResponse.transform(responseOrder.getPayment_profile()));
        coreOrderResponse.add(transform(responseOrder.getAmounts()));
        coreOrderResponse.setNumber(responseOrder.getNumber());
        coreOrderResponse.setState(responseOrder.getState());
        coreOrderResponse.setOrderItems(transform(responseOrder.getOrder_items()));
        coreOrderResponse.add(getCurrentCoreDeliveryTimeResponse());
        return coreOrderResponse;
    }

    public static CoreAmountsResponse transform(Amounts amounts) {
        CoreAmountsResponse coreAmountsResponse = new CoreAmountsResponse();
        coreAmountsResponse.setCoupon(String.valueOf(amounts.getCoupon()));
        coreAmountsResponse.setShipping(String.valueOf(amounts.getShipping()));
        coreAmountsResponse.setSubtotal(String.valueOf(amounts.getSubtotal()));
        coreAmountsResponse.setTax(String.valueOf(amounts.getTax()));
        coreAmountsResponse.setTip(String.valueOf(amounts.getTip()));
        coreAmountsResponse.setTotal(String.valueOf(amounts.getTotal()));
        transform(coreAmountsResponse,amounts.getDiscounts());

        return coreAmountsResponse;
    }

    public static void transform(CoreAmountsResponse coreAmountsResponse, Discounts discounts) {
        if (discounts == null) {
            return;
        }
        coreAmountsResponse.getDiscounts().setCoupons(String.valueOf(discounts.getCoupons()));
        coreAmountsResponse.getDiscounts().setDeals(String.valueOf(discounts.getDeals()));
    }


    public static CoreDeliveryTimeResponse getCurrentCoreDeliveryTimeResponse() {
        CoreDeliveryTimeResponse coreDeliveryTimeResponse = new CoreDeliveryTimeResponse();
        coreDeliveryTimeResponse.setDeliver_at(OffsetDateTime.now(ZoneId.of("UTC")));
        coreDeliveryTimeResponse.setLabel("1 Hour or less");
        return coreDeliveryTimeResponse;
    }

    public static ArrayList<CoreResponseOrderItems> transform(List<ResponseOrderItems> responseOrderItemsList) {
        if (responseOrderItemsList == null || responseOrderItemsList.isEmpty()) {
            return new ArrayList<>();
        }
        ArrayList<CoreResponseOrderItems> coreResponseOrderItemsArrayList = new ArrayList<>();
        for (ResponseOrderItems responseOrderItems : responseOrderItemsList) {
            CoreResponseOrderItems coreResponseOrderItems = new CoreResponseOrderItems();
            coreResponseOrderItems.setId(String.valueOf(responseOrderItems.getId()));
            coreResponseOrderItems.setProduct_id(responseOrderItems.getProduct_id());
            coreResponseOrderItems.setSupplier_id(String.valueOf(responseOrderItems.getSupplier_id()));
            coreResponseOrderItems.setName(responseOrderItems.getName());
            coreResponseOrderItems.setPrice(responseOrderItems.getPrice());
            coreResponseOrderItems.setQuantity(String.valueOf(responseOrderItems.getQuantity()));
            coreResponseOrderItems.setScheduled_for(responseOrderItems.getScheduled_for());
            coreResponseOrderItems.setTotal(responseOrderItems.getTotal());
            coreResponseOrderItems.setVolume(responseOrderItems.getVolume());
            coreResponseOrderItemsArrayList.add(coreResponseOrderItems);
        }
        return coreResponseOrderItemsArrayList;
    }

}
