package com.iconnect.vendor.minibar.services.transformers;

import com.iconnect.http.common.CoreAddressResponse;
import com.iconnect.http.response.CoreAddressListResponse;
import com.iconnect.vendor.minibar.minibar.common.Address;


import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;


public class AddressToResponse {

    private AddressToResponse(){

    }
 public static CoreAddressResponse transform(Address address){

        if(address == null){
            return null;
        }

        CoreAddressResponse coreAddressResponse = new CoreAddressResponse();
        coreAddressResponse.setId(String.valueOf(address.getId()));
        coreAddressResponse.setLocalId(address.getLocalID());

        if(address.getName() != null){

            coreAddressResponse.setLastname("");
            String name = address.getName();
            String[] names = name.split(" ");
            coreAddressResponse.setFirstname(names[0]);
            if(names.length>1){
                String lastname = Arrays.stream(names).skip(1).collect(Collectors.joining(" "));
                coreAddressResponse.setLastname(lastname);
            }

        }
     coreAddressResponse.setAddress1(address.getAddress1());
     coreAddressResponse.setAddress2(address.getAddress2());
     coreAddressResponse.setCompany(address.getCompany());
     coreAddressResponse.setCity(address.getCity());
     coreAddressResponse.setState(address.getState());
     coreAddressResponse.setZipcode(address.getZipcode());
     coreAddressResponse.setPhone(address.getPhone());
     coreAddressResponse.setLatitude(String.valueOf(address.getLatitude()));
     coreAddressResponse.setLongitude(String.valueOf(address.getLongitude()));
     coreAddressResponse.setIsDefault(address.getIsDefault());
     return coreAddressResponse;

 }

 public static CoreAddressListResponse transform(List<Address> addressList){
        if(addressList == null || addressList.isEmpty()){
            return new CoreAddressListResponse();
        }
        CoreAddressListResponse coreAddressListResponse = new CoreAddressListResponse();
        for(Address address : addressList){
            coreAddressListResponse.add(transform(address));
        }
        return coreAddressListResponse;
 }
}
