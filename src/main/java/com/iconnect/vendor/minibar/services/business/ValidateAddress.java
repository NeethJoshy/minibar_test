package com.iconnect.vendor.minibar.services.business;

import com.iconnect.http.common.CoreAddressRequest;
import com.iconnect.http.common.CoreAddressResponse;
import com.iconnect.http.request.CoreOrderRequest;
import com.iconnect.http.response.CoreAddressListResponse;
import com.iconnect.vendor.minibar.Configurations.Constants;
import com.iconnect.vendor.minibar.core.CurrentRequestScope;
import com.iconnect.vendor.minibar.core.CustomObjectMapper;
import com.iconnect.vendor.minibar.entity.AddressDetails;
import com.iconnect.vendor.minibar.minibar.response.AuthenticatedUser;
import com.iconnect.vendor.minibar.services.database.company.CustomerAddressDetailsService;
import com.iconnect.vendor.minibar.services.transformers.AddressToResponse;
import com.iconnect.vendor.minibar.services.transformers.ToCoreAddressRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.util.ArrayList;

@Service
public class ValidateAddress {

    @Autowired
    MinibarCoreLogic minibarCoreLogic;

    @Autowired
    CustomerAddressDetailsService customerAddressDetailService;

    @Autowired
    @Qualifier("internalRestTemplate")
    RestTemplate internalRestTemplate;

    private CustomObjectMapper objectMapper = new CustomObjectMapper();

    private HttpHeaders getHeader() {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        return headers;
    }

    public String fetchAddressID(CoreOrderRequest coreOrderRequest) throws IOException {
        AuthenticatedUser authenticatedUser = minibarCoreLogic.fetchUserDetails(CurrentRequestScope.getCustomerID());
        CoreAddressListResponse coreAddressListResponse =  AddressToResponse.transform(authenticatedUser.getShipping_addresses());
        ArrayList<AddressDetails> addressDetails = customerAddressDetailService.findALL(CurrentRequestScope.getCustomerID());

        String addressID = coreOrderRequest.getShippingId();
        boolean fromInternalAddress = false;
        AddressDetails internalAddress = null;
        for (AddressDetails addressDetail: addressDetails) {
            if(addressDetail.getInternalAddressID().equalsIgnoreCase(addressID)) { //get the vendorAddressID
                addressID = addressDetail.getVendorAddressID();
                internalAddress = addressDetail;
                fromInternalAddress = true;
                break;
            }
        }

        if(fromInternalAddress) {
            boolean isAddressValid = false;

            for (CoreAddressResponse coreAddressResponse : coreAddressListResponse.getAddress()) {
                if(coreAddressResponse.getId().equalsIgnoreCase(addressID)) {
                    isAddressValid = true;
                    break;
                }
            }

            if(isAddressValid) {
                return addressID;
            }

            //create the Address on the vendor
            CoreAddressRequest coreAddressRequest = ToCoreAddressRequest.transform(internalAddress);

            customerAddressDetailService.delete(internalAddress);
            CoreAddressResponse coreAddressResponse = minibarCoreLogic.createAddress(coreAddressRequest);
            return coreAddressResponse.getId();
        }

        //This addressID is not System generated but from the vendors.
        if(!addressID.toLowerCase().contains(Constants.ADDRESS_NAMESPACE.toLowerCase())) {
            return addressID;
        }

        CoreAddressResponse coreAddressResponse = fetchInternalAddress(addressID);

        if(coreAddressResponse == null) {// this addressID does not exist on our system
            return addressID;
        }

        CoreAddressRequest coreAddressRequest = ToCoreAddressRequest.transform(coreAddressResponse);
        coreAddressRequest.setInternalAddressID(addressID);

        CoreAddressResponse coreAddressResponseFromVendor = minibarCoreLogic.createAddress(coreAddressRequest);

        return coreAddressResponseFromVendor.getId();

    }

    private CoreAddressResponse fetchInternalAddress(String addressID) throws IOException {
        String url = Constants.CUSTOMER_MICROSERVICE_ADDRESS + "/" + addressID;
        HttpEntity entity = new HttpEntity<>(getHeader());
        try {
            ResponseEntity<String> responseEntity = internalRestTemplate.exchange(url, HttpMethod.GET, entity, String.class);
            return objectMapper.readValue(responseEntity.getBody(),CoreAddressResponse.class);
        }
        catch (HttpClientErrorException httpClientErrorException) {
            if(httpClientErrorException.getStatusCode() != HttpStatus.NOT_FOUND) {
                throw httpClientErrorException;
            }
        }
        return null;
    }
}
