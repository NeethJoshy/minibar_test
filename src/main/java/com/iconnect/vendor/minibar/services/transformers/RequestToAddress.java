package com.iconnect.vendor.minibar.services.transformers;

import com.iconnect.http.common.CoreAddressRequest;
import com.iconnect.vendor.minibar.minibar.common.Address;

public class RequestToAddress {
    private RequestToAddress(){

    }

    public static Address transform(CoreAddressRequest coreAddressRequest){
        Address address = new Address();
        address.setName(coreAddressRequest.getFirstName() + " " + coreAddressRequest.getLastName());
        address.setAddress1(coreAddressRequest.getAddress1());
        address.setAddress2(coreAddressRequest.getAddress2());
        address.setCity(coreAddressRequest.getCity());
        address.setCompany(coreAddressRequest.getCompany());
        address.setPhone(coreAddressRequest.getPhone());
        address.setState(coreAddressRequest.getState());
        address.setZipcode(coreAddressRequest.getZipcode());
        return address;
    }
}
