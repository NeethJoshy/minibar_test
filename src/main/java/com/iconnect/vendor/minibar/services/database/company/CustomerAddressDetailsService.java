package com.iconnect.vendor.minibar.services.database.company;

import com.iconnect.vendor.minibar.entity.AddressDetails;
import com.iconnect.vendor.minibar.repo.AddressDetailsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Service
public class CustomerAddressDetailsService extends EntityManagerServiceCompany {

    @Autowired
    AddressDetailsRepository addressDetailsRepository;

    @Transactional
    public ArrayList<AddressDetails> findALL(String customerId){
        return new ArrayList<>(addressDetailsRepository.findAllByCustomerID(customerId));
    }
    @Transactional
    public void save(List<AddressDetails> addressDetails) {
        addressDetailsRepository.saveAll(addressDetails);
    }

    @Transactional
    public AddressDetails findByVendorAddressIDAndCustomerID(String vendorID,String customerID) {
        return addressDetailsRepository.findByVendorAddressIDAndCustomerID(vendorID,customerID);
    }

    @Transactional
    public AddressDetails findByInternalAddressIDAndCustomerID(String internalAddressID,String customerID){
        return addressDetailsRepository.findByInternalAddressIDAndCustomerID(internalAddressID,customerID);
    }
    @Transactional
    public void delete(AddressDetails addressDetails){
        addressDetailsRepository.delete(addressDetails);
    }

    @Transactional
    public void save(AddressDetails addressDetail) {
        addressDetailsRepository.save(addressDetail);
    }

}
