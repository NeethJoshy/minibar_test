package com.iconnect.vendor.minibar.services.business;

import com.iconnect.vendor.minibar.core.CurrentRequestScope;
import com.iconnect.vendor.minibar.minibar.request.SuppliersList;
import com.iconnect.vendor.minibar.minibar.response.supplier.ResponseSupplierList;
import com.iconnect.vendor.minibar.minibar.response.supplier.Suppliers;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
public class MinibarToMinibarTransformers {
    private MinibarToMinibarTransformers(){

    }
    public SuppliersList getSuppliersList(ResponseSupplierList responseSupplierList){
        SuppliersList suppliersList = new SuppliersList();
        suppliersList.setBrands(CurrentRequestScope.getProductModel().getProducts().getCurrentBrands());

        ArrayList<String> supplierArray = new ArrayList<>();
        for(Suppliers suppliers:responseSupplierList.getSuppliers()){
            supplierArray.add(String.valueOf(suppliers.getId()));
        }
        suppliersList.setSuppliersLists(supplierArray);
        return suppliersList;
    }

}
