package com.iconnect.vendor.minibar.services.transformers;

import com.iconnect.http.common.CoreAddressResponse;
import com.iconnect.vendor.minibar.core.CurrentRequestScope;
import com.iconnect.vendor.minibar.entity.AddressDetails;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ToAddressDetails {
    private ToAddressDetails(){

    }

    public static AddressDetails transform(CoreAddressResponse coreAddressResponse){
        AddressDetails addressDetails = new AddressDetails();

        addressDetails.setInternalAddressID(coreAddressResponse.getInternalAddressId());
        addressDetails.setVendorAddressID(coreAddressResponse.getId());
        addressDetails.setCustomerID(CurrentRequestScope.getCustomerID());
        addressDetails.setFirstName(coreAddressResponse.getFirstname());
        addressDetails.setLastName(coreAddressResponse.getLastname());
        addressDetails.setLatitude(coreAddressResponse.getLatitude());
        addressDetails.setLongitude(coreAddressResponse.getLongitude());
        addressDetails.setAddress1(coreAddressResponse.getAddress1());
        addressDetails.setAddress2(coreAddressResponse.getAddress2());
        addressDetails.setCity(coreAddressResponse.getCity());
        addressDetails.setCompany(coreAddressResponse.getCompany());
        addressDetails.setState(coreAddressResponse.getState());
        addressDetails.setZipcode(coreAddressResponse.getZipcode());
        addressDetails.setPhone(coreAddressResponse.getPhone());
        return addressDetails;
    }

    public static List<AddressDetails> transform(HashMap<String,CoreAddressResponse> stringCoreAddressResponseHashMap){
        List<AddressDetails> addressDetails = new ArrayList<>();
        for(Map.Entry<String,CoreAddressResponse> entry: stringCoreAddressResponseHashMap.entrySet()){
            addressDetails.add(transform(entry.getValue()));
        }
        return addressDetails;
    }
}
