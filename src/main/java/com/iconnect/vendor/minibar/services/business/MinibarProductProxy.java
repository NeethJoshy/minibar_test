package com.iconnect.vendor.minibar.services.business;

import com.iconnect.vendor.minibar.model.ProductModel;
import com.iconnect.vendor.minibar.model.Products;
import com.iconnect.vendor.minibar.services.database.tenant.MinibarProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

@Service
public class MinibarProductProxy {
    public static final  String PRODUCT_PROXY_IDENTIFIER = "_PRODUCT_PROXY";

    public static final String FETCH_PRODUCT_MODEL = "_FETCH_PRODUCT_MODEL";
    @Autowired
    MinibarProductService minibarProductService;

  //  @Cacheable(value = Constants.NAMESPACE + PRODUCT_PROXY_IDENTIFIER, key = "#root.target.FETCH_PRODUCT_MODEL", cacheResolver = "cacheResolverExtended", sync = false)//TODO: enable sync
    public ProductModel fetchProductModel() {
        Products products = new Products();
        products.add(minibarProductService.fetchALL());
        ProductModel productModel = new ProductModel();
        productModel.setProducts(products);
        return productModel;
    }
}
