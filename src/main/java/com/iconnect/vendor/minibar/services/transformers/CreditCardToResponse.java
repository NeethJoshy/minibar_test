package com.iconnect.vendor.minibar.services.transformers;

import com.iconnect.http.response.CoreBillingListResponse;
import com.iconnect.http.response.CoreBillingResponse;
import com.iconnect.vendor.minibar.minibar.response.CreditCard;

import java.util.List;

public class CreditCardToResponse {
    private CreditCardToResponse(){

    }
    public static CoreBillingListResponse transform(List<CreditCard> creditCardList) {
        if (creditCardList == null || creditCardList.isEmpty()) {
            return new CoreBillingListResponse();
        }
        CoreBillingListResponse coreBillingListResponse = new CoreBillingListResponse();

        for (CreditCard creditCard : creditCardList) {
            coreBillingListResponse.add(transform(creditCard));
        }
        return coreBillingListResponse;
    }

    public static CoreBillingResponse transform(CreditCard creditCard) {
        CoreBillingResponse coreBillingResponse = new CoreBillingResponse();

        coreBillingResponse.setId(String.valueOf(creditCard.getId()));
        coreBillingResponse.setCardType(creditCard.getCc_card_type());
        coreBillingResponse.setLastFour(creditCard.getCc_last_four());
        coreBillingResponse.setExpMonth(creditCard.getCc_exp_month());
        coreBillingResponse.setExpYear(creditCard.getCc_exp_year());
        coreBillingResponse.setIsDefault(creditCard.getIs_default());
        coreBillingResponse.setAddress(AddressToResponse.transform(creditCard.getAddress()));
        return coreBillingResponse;
    }
}
