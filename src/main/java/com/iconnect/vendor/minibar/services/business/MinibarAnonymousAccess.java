package com.iconnect.vendor.minibar.services.business;

import com.iconnect.vendor.minibar.Configurations.Constants;
import com.iconnect.vendor.minibar.core.CurrentRequestScope;
import com.iconnect.vendor.minibar.entity.MinibarExternalAuthentication;
import com.iconnect.vendor.minibar.filter.HttpHeaderFilter;
import com.iconnect.vendor.minibar.minibar.response.AccessToken;
import com.iconnect.vendor.minibar.services.database.tenant.MinibarExternalAuthenticationService;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
public class MinibarAnonymousAccess {

    @Autowired
    MinibarExternalAuthenticationService minibarExternalAuthenticationService;

    @Autowired
    MinibarHttpProxy minibarHttpProxy;

    private static final org.slf4j.Logger log = LoggerFactory.getLogger(HttpHeaderFilter.class);

    public AccessToken fetchAccessToken() throws IOException {

        log.info("+++++++++++++++" + Constants.MINIBAR.ACCESS_TOKEN());
        String tenant = CurrentRequestScope.getCurrentTenant();
        MinibarExternalAuthentication mea = minibarExternalAuthenticationService.getTenantExternalAuthenticationDetails();
        AccessToken accessToken = minibarHttpProxy.fetchAccessToken(mea);
        log.info("{}",accessToken);
        return accessToken;
    }
}
