package com.iconnect.vendor.minibar.services.transformers;

import com.iconnect.http.request.CoreBillingRequest;
import com.iconnect.vendor.minibar.minibar.request.CreateCreditCard;

public class RequestToCreditCard {
    private  RequestToCreditCard(){}
    public static CreateCreditCard transform(CoreBillingRequest coreBillingRequest) {
        CreateCreditCard createCreditCard = new CreateCreditCard();
        createCreditCard.setPayment_method_nonce(coreBillingRequest.getPaymentMethodNonce());
        createCreditCard.setAddress(RequestToAddress.transform(coreBillingRequest.getAddress()));
        return createCreditCard;
    }
}
