package com.iconnect.vendor.minibar.services.transformers;

import com.iconnect.http.response.product.CoreDeliveryMethodResponse;
import com.iconnect.http.response.product.CoreHoursResponse;
import com.iconnect.http.response.product.CoreNextSchedulingWindowResponse;
import com.iconnect.vendor.minibar.minibar.response.supplier.Delivery_method;
import com.iconnect.vendor.minibar.minibar.response.supplier.Hours;
import com.iconnect.vendor.minibar.minibar.response.supplier.Next_scheduling_window;


import java.util.ArrayList;
import java.util.List;


public class DeliveryMethodToResponse {

    public DeliveryMethodToResponse(){

    }

    public static ArrayList<CoreDeliveryMethodResponse> transform(List<Delivery_method> deliveryMethods){

        if(deliveryMethods == null){
            return new ArrayList<>();
        }

        ArrayList<CoreDeliveryMethodResponse> coreDeliveryMethodResponses = new ArrayList<>();

        for(Delivery_method delivery_method : deliveryMethods ){
            CoreDeliveryMethodResponse coreDeliveryMethodResponse = new CoreDeliveryMethodResponse();
            coreDeliveryMethodResponse.setIsDefault(delivery_method.getIsDefault());
            coreDeliveryMethodResponse.setAllowsScheduling(delivery_method.getAllowsScheduling());
            coreDeliveryMethodResponse.setAllowsTipping(delivery_method.getAllowsTipping());
            coreDeliveryMethodResponse.setDelivery_expectation(delivery_method.getDelivery_expectation());
            coreDeliveryMethodResponse.setDelivery_fee(delivery_method.getDelivery_fee());
            coreDeliveryMethodResponse.setDelivery_minimum(delivery_method.getDelivery_minimum());
            coreDeliveryMethodResponse.setFree_delivery_threshold(delivery_method.getFree_delivery_threshold());
            coreDeliveryMethodResponse.setHours(transformHours(delivery_method.getHours()));
            coreDeliveryMethodResponse.setMaximum_delivery_expectation(delivery_method.getMaximum_delivery_expectation());
            coreDeliveryMethodResponse.setSupplier_id(delivery_method.getSupplier_id());
            coreDeliveryMethodResponse.setName(delivery_method.getName());
            coreDeliveryMethodResponse.setNext_delivery(delivery_method.getNext_delivery());
            coreDeliveryMethodResponse.setPer_item_delivery_fee(delivery_method.getPer_item_delivery_fee());
            coreDeliveryMethodResponse.setType(delivery_method.getType());
            coreDeliveryMethodResponse.setScheduling_mode(delivery_method.getScheduling_mode());
            coreDeliveryMethodResponse.setNext_scheduling_window(transformSchedulingWindow(delivery_method.getNext_scheduling_window()));
            coreDeliveryMethodResponses.add(coreDeliveryMethodResponse);
        }

        return coreDeliveryMethodResponses;
    }

    private static CoreHoursResponse transformHours(Hours hours) {
        CoreHoursResponse hoursResponse = new CoreHoursResponse();
        hoursResponse.setAlways_open(hours.getAlways_open());
        hoursResponse.setOpens_at(hours.getOpens_at());
        hoursResponse.setCloses_at(hours.getCloses_at());
        return hoursResponse;
    }

    private static CoreNextSchedulingWindowResponse transformSchedulingWindow(Next_scheduling_window nextSchedulingWindow) {
        if (nextSchedulingWindow == null) {
            return null;
        }
        CoreNextSchedulingWindowResponse nextSchedulingWindowResponse = new CoreNextSchedulingWindowResponse();
        nextSchedulingWindowResponse.setStart_time(nextSchedulingWindow.getStart_time());
        nextSchedulingWindowResponse.setEnd_time(nextSchedulingWindow.getEnd_time());
        return nextSchedulingWindowResponse;
    }
}
