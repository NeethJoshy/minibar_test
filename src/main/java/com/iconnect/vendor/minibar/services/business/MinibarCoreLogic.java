package com.iconnect.vendor.minibar.services.business;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.iconnect.http.common.CoreAddressRequest;
import com.iconnect.http.common.CoreAddressResponse;
import com.iconnect.http.request.CoreAddressRequestProductList;
import com.iconnect.http.request.CoreBillingRequest;
import com.iconnect.http.request.CoreDeliverAtRequest;
import com.iconnect.http.request.CoreOrderRequest;
import com.iconnect.http.response.*;
import com.iconnect.http.response.order.CoreOrderResponseContainer;
import com.iconnect.http.response.order.CoreResponseOrderItems;
import com.iconnect.http.response.product.CorePriceListResponse;
import com.iconnect.http.response.product.CoreSupplierResponse;
import com.iconnect.rowjson.RawObject;
import com.iconnect.vendor.minibar.Configurations.Constants;
import com.iconnect.vendor.minibar.core.CurrentRequestScope;
import com.iconnect.vendor.minibar.core.CustomObjectMapper;
import com.iconnect.vendor.minibar.entity.AddressDetails;
import com.iconnect.vendor.minibar.entity.MinibarCustomerAuthentication;
import com.iconnect.vendor.minibar.entity.MinibarOrder;
import com.iconnect.vendor.minibar.minibar.request.SuppliersList;
import com.iconnect.vendor.minibar.minibar.request.SuppliersParam;
import com.iconnect.vendor.minibar.minibar.response.AccessToken;
import com.iconnect.vendor.minibar.minibar.response.AuthenticatedUser;
import com.iconnect.vendor.minibar.minibar.response.OauthToken;
import com.iconnect.vendor.minibar.minibar.response.order.ResponseOrder;
import com.iconnect.vendor.minibar.minibar.response.order.ResponseOrderFinalized;
import com.iconnect.vendor.minibar.minibar.response.product.ProductList;
import com.iconnect.vendor.minibar.minibar.response.supplier.ResponseSupplierList;
import com.iconnect.vendor.minibar.minibar.response.supplier.Suppliers;
import com.iconnect.vendor.minibar.services.database.company.CustomerAddressDetailsService;
import com.iconnect.vendor.minibar.services.database.company.MinibarCustomerAuthenticationService;
import com.iconnect.vendor.minibar.services.database.tenant.MinibarOrderService;
import com.iconnect.vendor.minibar.services.transformers.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.UUID;

@Service
public class MinibarCoreLogic {

    @Autowired
    MinibarAnonymousAccess minibarAnonymousAccess;
    @Autowired
    MinibarHttpProxy minibarHttpProxy;
    @Autowired
    MinibarToMinibarTransformers minibarToMinibarTransformers;
    @Autowired
    ProductListToResponse productListToResponse;
    @Autowired
    MinibarAuthenticatedAccess minibarAuthenticatedAccess;
    @Autowired
    MinibarCustomerAuthenticationService minibarCustomerAuthenticationService;
    @Autowired
    CustomerAddressDetailsService customerAddressDetailsService;
    @Autowired
    ValidateAddress validateAddress;
    @Autowired
    MinibarOrderService minibarOrderService;

    private CustomObjectMapper objectMapper = new CustomObjectMapper();

   public void fetchAndPopulateAccessToken() throws IOException{

       AccessToken accessToken = minibarAnonymousAccess.fetchAccessToken();
       CurrentRequestScope.setAccessToken(accessToken.getAccess_token());

    }

    public OauthToken fetchAndPopulateUserToken() throws IOException{
        OauthToken oauthToken = minibarAuthenticatedAccess.fetchUserToken(CurrentRequestScope.getCustomerID());
        CurrentRequestScope.setUserToken(oauthToken.getAccess_token());
        return oauthToken;
    }

    public AuthenticatedUser fetchUserDetails(String customer_id) throws IOException{
        fetchAndPopulateUserToken();
        MinibarCustomerAuthentication mca = minibarCustomerAuthenticationService.getCustomerDetails(customer_id);
        AuthenticatedUser authenticatedUser = minibarAuthenticatedAccess.fetchAccountDetails(customer_id);
        if (mca != null && !authenticatedUser.getId().equalsIgnoreCase(mca.getUserID())) {
            mca.setUserID(authenticatedUser.getId());
            minibarCustomerAuthenticationService.save(customer_id, mca);
        }
        return authenticatedUser;
    }

    public String defaultResponse() throws JsonProcessingException {
       DefaultResponse defautlResponse = new DefaultResponse();
       defautlResponse.setMessage("success");
       return objectMapper.writeValueAsString(defautlResponse);
    }

    public CoreProductListResponse getProducts(CoreAddressRequestProductList address) throws IOException
    {
       fetchAndPopulateAccessToken();
       SuppliersParam suppliersParam = CoreToMinibarTransformers.convertAddressRequest(address);
       ResponseSupplierList responseSupplierList = minibarHttpProxy.getSuppliers(suppliersParam);
       SuppliersList suppliersList = minibarToMinibarTransformers.getSuppliersList(responseSupplierList);
       ProductList productList = minibarHttpProxy.getProducts(suppliersList);
       return productListToResponse.transform(responseSupplierList,productList);
    }

    public CorePriceListResponse getProducts(String productId) throws IOException{
       fetchAndPopulateAccessToken();
       String permalink = CurrentRequestScope.getProductModel().getProducts().getPermalinkId(productId);
       return ProductGroupingToPriceRange.transfom(minibarHttpProxy.getProducts(permalink));
    }

    public CoreAddressListResponse getAddress() throws IOException{
       AuthenticatedUser authenticatedUser = fetchUserDetails(CurrentRequestScope.getCustomerID());
       CoreAddressListResponse coreAddressListResponse =  AddressToResponse.transform(authenticatedUser.getShipping_addresses());
       ArrayList<AddressDetails> addressDetails = customerAddressDetailsService.findALL(CurrentRequestScope.getCustomerID());
       HashMap<String, CoreAddressResponse> stringCoreAddressResponseHashMap = new HashMap<>();
       for(CoreAddressResponse coreAddressResponse:coreAddressListResponse.getAddress()){
           final String internalAddressId = Constants.ADDRESS_NAMESPACE + UUID.randomUUID().toString();
           coreAddressResponse.setInternalAddressId(internalAddressId);
           stringCoreAddressResponseHashMap.put(coreAddressResponse.getId(),coreAddressResponse);
           for(AddressDetails addressDetail: addressDetails){
               if(coreAddressResponse.getId().equalsIgnoreCase(addressDetail.getVendorAddressID())){
                   coreAddressResponse.setInternalAddressId(addressDetail.getInternalAddressID());
                   stringCoreAddressResponseHashMap.remove(addressDetail.getVendorAddressID());
                   break;
               }
           }
       }
       if(!stringCoreAddressResponseHashMap.isEmpty())

       customerAddressDetailsService.save(ToAddressDetails.transform(stringCoreAddressResponseHashMap));
       return coreAddressListResponse;
    }

    public CoreAddressResponse createAddress(CoreAddressRequest coreAddressRequest) throws IOException{
       fetchAndPopulateUserToken();
       final String internalAddressId = coreAddressRequest.getInternalAddressID();
       CoreAddressResponse coreAddressResponse = AddressToResponse.transform(minibarHttpProxy.createAddress(RequestToAddress.transform(coreAddressRequest)));
       AddressDetails existingAddress = customerAddressDetailsService.findByVendorAddressIDAndCustomerID(coreAddressResponse.getId(),CurrentRequestScope.getCustomerID());
       if(existingAddress == null){
           coreAddressResponse.setInternalAddressId(internalAddressId);
           AddressDetails addressDetails = new AddressDetails();
           addressDetails.setInternalAddressID(internalAddressId);
           addressDetails.setVendorAddressID(coreAddressResponse.getId());
           addressDetails.setCustomerID(CurrentRequestScope.getCustomerID());
           addressDetails.setFirstName(coreAddressResponse.getFirstname());
           addressDetails.setLastName(coreAddressResponse.getLastname());
           addressDetails.setLatitude(coreAddressResponse.getLatitude());
           addressDetails.setLongitude(coreAddressResponse.getLongitude());
           addressDetails.setAddress1(coreAddressResponse.getAddress1());
           addressDetails.setAddress2(coreAddressRequest.getAddress2());
           addressDetails.setCompany(coreAddressResponse.getCompany());
           addressDetails.setCity(coreAddressResponse.getCity());
           addressDetails.setState(coreAddressResponse.getState());
           addressDetails.setPhone(coreAddressResponse.getPhone());
           addressDetails.setZipcode(coreAddressResponse.getZipcode());
           customerAddressDetailsService.save(addressDetails);
       }
       else{
           coreAddressResponse.setInternalAddressId(existingAddress.getInternalAddressID());
       }
      // resetUserAccountDetailsCache();
       return coreAddressResponse;
    }

    public String deleteAddress(String addressID) throws IOException{
       fetchAndPopulateUserToken();
       AddressDetails addressDetails = customerAddressDetailsService.findByInternalAddressIDAndCustomerID(addressID,CurrentRequestScope.getCustomerID());
       if(addressDetails != null){
           addressID = addressDetails.getVendorAddressID();
           customerAddressDetailsService.delete(addressDetails);
       }
       minibarHttpProxy.deleteAddress(addressID);
       return defaultResponse();
    }

    public CoreBillingListResponse getBillingInfo() throws IOException{
        AuthenticatedUser authenticatedUser = fetchUserDetails(CurrentRequestScope.getCustomerID());
        return CreditCardToResponse.transform(authenticatedUser.getPayment_profiles());
    }


    public CoreBillingResponse createBillingInfo(CoreBillingRequest billingRequest) throws IOException {
        fetchAndPopulateUserToken();
        CoreBillingResponse coreBillingResponse = CreditCardToResponse.transform(minibarHttpProxy.createCreditCard(RequestToCreditCard.transform(billingRequest)));
        return coreBillingResponse;
    }

    public String deleteBillingInfo(String billingID) throws IOException {
        fetchAndPopulateUserToken();
        minibarHttpProxy.deleteCreditCard(billingID);
        return defaultResponse();
    }

    public CoreBillingTokenResponse fetchBillingToken() throws IOException {
        fetchAndPopulateAccessToken();
        return BillingTokenToResponse.transform(minibarHttpProxy.getBraintreeSDKToken());
    }

    public CoreOrderResponseContainer createOrder(CoreOrderRequest coreOrderRequest) throws IOException {
        fetchAndPopulateUserToken();
        coreOrderRequest.setShippingId(validateAddress.fetchAddressID(coreOrderRequest));
        ResponseOrder responseOrder = minibarHttpProxy.createOrder(RequestToOrder.transform(coreOrderRequest));
        CoreOrderResponseContainer coreOrderResponseContainer = OrderToResponse.transform(responseOrder);
        populateOrderSupplier(coreOrderResponseContainer);
        MinibarOrder minibarOrder = new MinibarOrder();
        minibarOrder.setCustomerID(CurrentRequestScope.getCustomerID());
        minibarOrder.setOrderRequest(objectMapper.writeValueAsString(coreOrderRequest));
        minibarOrder.setVendorOrderNumber(coreOrderResponseContainer.getOrder().getNumber());
        minibarOrder.setVendorOrderResponse(objectMapper.writeValueAsString(responseOrder));
        minibarOrderService.save(minibarOrder);
        RawObject vendorResponse = new RawObject(objectMapper.writeValueAsString(responseOrder));
        coreOrderResponseContainer.setVendorResponse(vendorResponse);

        return coreOrderResponseContainer;
    }


    public CoreOrderResponseContainer finalizeOrder(String orderNumber, CoreDeliverAtRequest coreDeliverAtRequest) throws IOException{
       fetchAndPopulateUserToken();
       MinibarOrder minibarOrder = minibarOrderService.findByCustomerIDAndVendorOrderNumber(CurrentRequestScope.getCustomerID(),orderNumber);
       if (minibarOrder.isFinalized()){
           ResponseOrderFinalized responseOrderFinalized = objectMapper.readValue(minibarOrder.getVendorOrderFinalizedResponse(),ResponseOrderFinalized.class);
           CoreOrderResponseContainer coreOrderResponseContainer = OrderToResponse.transform(responseOrderFinalized);
           populateOrderSupplier(coreOrderResponseContainer);
           RawObject vendorResponse = new RawObject(objectMapper.writeValueAsString(responseOrderFinalized));
           coreOrderResponseContainer.setVendorResponse(vendorResponse);
           return coreOrderResponseContainer;
       }
//TODO coreDeliverAtRequest null case
       minibarOrder.setOrderSubmitted(1);
       minibarOrderService.save(minibarOrder);

       ResponseOrderFinalized responseOrderFinalized = minibarHttpProxy.finalizeOrder(orderNumber);
       CoreOrderResponseContainer coreOrderResponseContainer = OrderToResponse.transform(responseOrderFinalized);
       populateOrderSupplier(coreOrderResponseContainer);

       minibarOrder.setVendorOrderFinalizedResponse(objectMapper.writeValueAsString(responseOrderFinalized));
       minibarOrder.setHasFinalized(1);
       minibarOrderService.save(minibarOrder);
       RawObject vendorResponse = new RawObject(objectMapper.writeValueAsString(responseOrderFinalized));
       coreOrderResponseContainer.setVendorResponse(vendorResponse);

       return coreOrderResponseContainer;
    }

    private CoreOrderResponseContainer populateOrderSupplier(CoreOrderResponseContainer coreOrderResponseContainer) throws IOException {
        fetchAndPopulateAccessToken();
        CoreAddressRequestProductList address = new CoreAddressRequestProductList();
        address.setLatitude(coreOrderResponseContainer.getOrder().getShippingAddress().getLatitude());
        address.setLongitude(coreOrderResponseContainer.getOrder().getShippingAddress().getLongitude());
        address.setZipcode(coreOrderResponseContainer.getOrder().getShippingAddress().getZipcode());
        SuppliersParam suppliersParam = CoreToMinibarTransformers.convertAddressRequest(address);
        ResponseSupplierList responseSupplierList = minibarHttpProxy.getSuppliers(suppliersParam);

        HashSet<String> supplierSet = new HashSet<>();
        ArrayList<CoreSupplierResponse> coreSupplierList = new ArrayList<>();

        for (CoreResponseOrderItems coreResponseOrderItems : coreOrderResponseContainer.getOrder().getOrderItems()) {
            supplierSet.add(coreResponseOrderItems.getSupplier_id());
        }

        for (Suppliers supplier : responseSupplierList.getSuppliers()) {
            if (!supplierSet.contains(String.valueOf(supplier.getId()))) {
                continue;
            }
            coreSupplierList.add(ProductListToResponse.transform(supplier));
        }
        coreOrderResponseContainer.setSuppliers(coreSupplierList);

        MinibarCustomerAuthentication mca = minibarCustomerAuthenticationService.getCustomerDetails(CurrentRequestScope.getCustomerID());
        coreOrderResponseContainer.getOrder().setUserID(mca.getUserID());

        return coreOrderResponseContainer;
    }


}
