package com.iconnect.vendor.minibar.services.database.company;

import com.iconnect.vendor.minibar.entity.MinibarCustomerAuthentication;
import com.iconnect.vendor.minibar.repo.MinibarCustomerAuthenticationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
public class MinibarCustomerAuthenticationService extends EntityManagerServiceCompany {


    @Autowired
    MinibarCustomerAuthenticationRepository minibarCustomerAuthenticationRepository;

    @Transactional
    public MinibarCustomerAuthentication getCustomerDetails(String customerid) {
        return minibarCustomerAuthenticationRepository.findByCustomerid(customerid);
    }

    @Transactional
    public MinibarCustomerAuthentication save(String customerId,MinibarCustomerAuthentication minibarCustomerAuthentication){
        minibarCustomerAuthenticationRepository.save(minibarCustomerAuthentication);
        return minibarCustomerAuthentication;
    }
}
