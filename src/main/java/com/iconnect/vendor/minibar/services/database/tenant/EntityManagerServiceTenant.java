package com.iconnect.vendor.minibar.services.database.tenant;

import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Service
public class EntityManagerServiceTenant {
    @PersistenceContext
    public EntityManager entityManager;
}
