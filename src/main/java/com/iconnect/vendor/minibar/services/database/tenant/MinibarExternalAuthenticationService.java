package com.iconnect.vendor.minibar.services.database.tenant;

import com.iconnect.vendor.minibar.entity.MinibarExternalAuthentication;
import com.iconnect.vendor.minibar.repo.MinibarExternalAuthenticationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
public class MinibarExternalAuthenticationService extends EntityManagerServiceTenant{


    @Autowired
    MinibarExternalAuthenticationRepository minibarExternalAuthenticationRepository;

    @Transactional
    public MinibarExternalAuthentication getTenantExternalAuthenticationDetails() {
        if (minibarExternalAuthenticationRepository.findAll().isEmpty()) {
            return null;
        }
        return minibarExternalAuthenticationRepository.findAll().iterator().next();
    }
}
