package com.iconnect.vendor.minibar.services.business;

import com.iconnect.vendor.minibar.core.CurrentRequestScope;
import com.iconnect.vendor.minibar.entity.MinibarCustomerAuthentication;
import com.iconnect.vendor.minibar.entity.MinibarExternalAuthentication;
import com.iconnect.vendor.minibar.minibar.request.CreateUser;
import com.iconnect.vendor.minibar.minibar.response.AuthenticatedUser;
import com.iconnect.vendor.minibar.minibar.response.OauthToken;
import com.iconnect.vendor.minibar.services.database.company.MinibarCustomerAuthenticationService;
import com.iconnect.vendor.minibar.services.database.tenant.MinibarExternalAuthenticationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
public class MinibarAuthenticatedAccess {

    private static final Logger log = LoggerFactory.getLogger(MinibarAuthenticatedAccess.class);

    @Autowired
    MinibarCustomerAuthenticationService minibarCustomerAuthenticationService;

    @Autowired
    MinibarExternalAuthenticationService minibarExternalAuthenticationService;

    @Autowired
    MinibarHttpProxy minibarHttpProxy;



    public OauthToken fetchUserToken(String customer) throws IOException{
        MinibarCustomerAuthentication mca = minibarCustomerAuthenticationService.getCustomerDetails(customer);
        MinibarExternalAuthentication mea = minibarExternalAuthenticationService.getTenantExternalAuthenticationDetails();
        if (mca == null) {
            log.error("No Customer Authentication for minibar available for Customer '{}' Company '{}' Tenant '{}' ", customer, CurrentRequestScope.getCurrentIdentifier(), CurrentRequestScope.getCurrentTenant());
            return null;
        }
        return loginUser(mea, mca);
    }

    private OauthToken loginUser(MinibarExternalAuthentication mea, MinibarCustomerAuthentication mca) throws IOException {
        CreateUser loginUser = new CreateUser();
        loginUser.setClientID(mea.getClient_id());
        loginUser.setClientSecret(mea.getClient_secret());
        loginUser.setEmail(mca.getUsername());
        loginUser.setPassword(mca.getPassword());
        OauthToken oauthToken = minibarHttpProxy.authenticateUserOauth(loginUser);
//        CacheOauthTokens cacheOauthTokens = new CacheOauthTokens();
//        cacheOauthTokens.setOauthToken(oauthToken);
//        cacheOauthTokens.setCacheKey(CurrentRequestScope.getCurrentTenant() + ":" + Constants.NAMESPACE + AUTHENTICATION_IDENTIFIER + ":" + USER_TOKEN + mca.getCustomerid());
//        upgradeCacheTokenTTL.post(cacheOauthTokens);
        return oauthToken;
    }

    public AuthenticatedUser fetchAccountDetails(String customer) throws IOException {
        return minibarHttpProxy.fetchUser();
    }
}
