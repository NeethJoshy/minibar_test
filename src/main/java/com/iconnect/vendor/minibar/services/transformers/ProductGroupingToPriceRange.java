package com.iconnect.vendor.minibar.services.transformers;

import com.iconnect.http.response.product.CorePriceListResponse;
import com.iconnect.http.response.product.CorePriceResponse;
import com.iconnect.vendor.minibar.minibar.response.product.ExternalProducts;
import com.iconnect.vendor.minibar.minibar.response.product.Product_grouping;

import java.math.BigDecimal;
import java.util.ArrayList;

public class ProductGroupingToPriceRange {
    public static CorePriceListResponse transfom(Product_grouping productGrouping) {
        ArrayList<ExternalProducts> externalProducts = productGrouping.getExternal_products();
        CorePriceResponse corePriceResponse = new CorePriceResponse();
        for (ExternalProducts externalProduct: externalProducts){
            if(corePriceResponse.getMax() == null){
                corePriceResponse.setMax(externalProduct.getMax_price());
            }

            if(corePriceResponse.getMin() == null){
                corePriceResponse.setMin(externalProduct.getMin_price());
            }

            if ((new BigDecimal(corePriceResponse.getMax())).compareTo(new BigDecimal(externalProduct.getMax_price())) == -1) {
                corePriceResponse.setMax(externalProduct.getMax_price());
            }

            if ((new BigDecimal(corePriceResponse.getMin())).compareTo(new BigDecimal(externalProduct.getMin_price())) == 1) {
                corePriceResponse.setMin(externalProduct.getMin_price());
            }
        }

        CorePriceListResponse corePriceListResponse = new CorePriceListResponse();
        ArrayList<CorePriceResponse> corePriceResponses = new ArrayList<>();
        corePriceResponses.add(corePriceResponse);
        corePriceListResponse.setPrices(corePriceResponses);
        return corePriceListResponse;
    }


}
