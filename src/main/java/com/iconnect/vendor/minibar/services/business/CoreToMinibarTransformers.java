package com.iconnect.vendor.minibar.services.business;

import com.iconnect.http.request.CoreAddressRequestProductList;
import com.iconnect.vendor.minibar.core.CurrentRequestScope;
import com.iconnect.vendor.minibar.minibar.request.Coords;
import com.iconnect.vendor.minibar.minibar.request.SuppliersParam;
import com.iconnect.vendor.minibar.model.ProductModel;
import com.iconnect.vendor.minibar.model.Products;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
public class CoreToMinibarTransformers {
    public static SuppliersParam convertAddressRequest(CoreAddressRequestProductList address) {
        SuppliersParam suppliersParam = new SuppliersParam();
        Coords coords = new Coords();

        coords.setLatitude(address.getLatitude());
        coords.setLongitude(address.getLongitude());

        suppliersParam.setCoords(coords);
        suppliersParam.setZipcode(address.getZipcode());

        ProductModel productModel = CurrentRequestScope.getProductModel();
        Products products = productModel.getProducts();
        ArrayList<String> productGroupingList = products.getBrandProductGrouping();

        suppliersParam.setProduct_grouping_ids(productGroupingList);

        return suppliersParam;
    }
}
