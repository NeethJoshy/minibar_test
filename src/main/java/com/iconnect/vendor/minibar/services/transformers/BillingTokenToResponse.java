package com.iconnect.vendor.minibar.services.transformers;

import com.iconnect.http.response.CoreBillingTokenResponse;
import com.iconnect.vendor.minibar.minibar.response.BraintreeToken;

public class BillingTokenToResponse {
    private BillingTokenToResponse(){

    }

    public static CoreBillingTokenResponse transform(BraintreeToken braintreeToken) {
        CoreBillingTokenResponse coreBillingTokenResponse = new CoreBillingTokenResponse();
        coreBillingTokenResponse.setClientToken(braintreeToken.getClient_token());
        return coreBillingTokenResponse;
    }
}
