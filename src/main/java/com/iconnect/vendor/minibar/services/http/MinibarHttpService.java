package com.iconnect.vendor.minibar.services.http;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.iconnect.vendor.minibar.Configurations.Constants;
import com.iconnect.vendor.minibar.core.CurrentRequestScope;
import com.iconnect.vendor.minibar.core.CustomObjectMapper;
import com.iconnect.vendor.minibar.entity.MinibarExternalAuthentication;
import com.iconnect.vendor.minibar.minibar.common.Address;
import com.iconnect.vendor.minibar.minibar.request.*;
import com.netflix.ribbon.proxy.annotation.Http;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.HashMap;
import java.util.Map;

@Service
public class MinibarHttpService {
    final RestTemplate restTemplate;

    HttpHeaders getAnonymousAccess() {
        HttpHeaders headers = getJsonHeader();
        headers.set(HttpHeaders.AUTHORIZATION, CurrentRequestScope.getBearerToken());
        return headers;
    }

    HttpHeaders getJsonHeader() {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        return headers;
    }

    HttpHeaders getAuthenticatedAccess() {
        HttpHeaders headers = getJsonHeader();
        headers.set(HttpHeaders.AUTHORIZATION, CurrentRequestScope.getUserToken());
        return headers;
    }


    @Autowired
    public MinibarHttpService(@Autowired RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    public ResponseEntity<String> fetchAccessToken(MinibarExternalAuthentication mea) {

        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(Constants.MINIBAR.ACCESS_TOKEN())
                // Add query parameter
                .queryParam(Constants.MINIBAR.CLIENT_ID_PARAM, mea.getClient_id())
                .queryParam(Constants.MINIBAR.CLIENT_SECRET_PARAM, mea.getClient_secret())
                .queryParam(Constants.MINIBAR.GRANT_TYPE_PARAM, Constants.MINIBAR.GRANT_TYPE_VALUE);

        return restTemplate.postForEntity(builder.toUriString(), null, String.class);
    }

    public ResponseEntity<String> getSuppliers(SuppliersParam suppliersParam) {
        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(Constants.MINIBAR.V2.GET_SUPPLIERS())
                .queryParam(Constants.MINIBAR.COORDS_LATITUDE, suppliersParam.getCoords().getLatitude())
                .queryParam(Constants.MINIBAR.COORDS_LONGITUDE, suppliersParam.getCoords().getLongitude());
        for (String product_grouping_id : suppliersParam.getProduct_grouping_ids()) {
            builder.queryParam(Constants.MINIBAR.ROUTING_OPTIONS_PRODUCT_GROUPING, product_grouping_id);
        }

        HttpHeaders headers = getAnonymousAccess();
        HttpEntity entity = new HttpEntity(headers);

        return restTemplate.exchange(
                builder.build(false).toUriString(), HttpMethod.GET, entity, String.class);
    }


    public ResponseEntity<String> getProducts(SuppliersList suppliersList){
        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(Constants.MINIBAR.V2.GET_PRODUCTS());
        Map<String,String> uriParams = new HashMap<>();
        uriParams.put(Constants.MINIBAR.GET_PRODUCTS_PLACEHOLDER, String.join(",", suppliersList.getSuppliersLists()));
        if (suppliersList.hasBrands()) {
            builder.queryParam(Constants.MINIBAR.MINIBAR_GLOBAL_QUERY, suppliersList.getBrands());
        }

        HttpHeaders headers = getAnonymousAccess();
        HttpEntity entity = new HttpEntity(headers);

        return restTemplate.exchange(
                builder.buildAndExpand(uriParams).toUriString(), HttpMethod.GET,entity,String.class);
    }

    public ResponseEntity<String> getProducts(String permalink){
        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(Constants.MINIBAR.V2.GET_PRODUCTS_PRICE_RANGE(permalink));


        HttpHeaders headers = getAnonymousAccess();
        HttpEntity entity = new HttpEntity(headers);

        return restTemplate.exchange(
                builder.build(false).toUriString(),HttpMethod.GET,entity,String.class);
    }

    public ResponseEntity<String> authenticateUserOauth(CreateUser createUser) throws JsonProcessingException {

        HttpHeaders headers = getJsonHeader();

        CustomObjectMapper objectMapper = new CustomObjectMapper();
        String requestJson = objectMapper.writeValueAsString(createUser);

        HttpEntity<String> entity = new HttpEntity<>(requestJson, headers);

        return restTemplate.postForEntity(Constants.MINIBAR.V2.AUTHENTICATE_OAUTH(), entity, String.class);
    }

    public ResponseEntity<String> fetchUser() {
        HttpHeaders headers = getAuthenticatedAccess();
        HttpEntity entity = new HttpEntity(headers);
        return restTemplate.exchange(Constants.MINIBAR.V2.CREATE_USER(), HttpMethod.GET, entity, String.class);
    }

    public ResponseEntity<String> createAddress(Address address) throws JsonProcessingException {
        HttpHeaders headers = getAuthenticatedAccess();
        CustomObjectMapper objectMapper = new CustomObjectMapper();
        String requestJson = objectMapper.writeValueAsString(address);

        HttpEntity<String> entity = new HttpEntity<>(requestJson, headers);

        return restTemplate.exchange(Constants.MINIBAR.V2.CREATE_ADDRESS(), HttpMethod.POST, entity, String.class);
    }
    public ResponseEntity<String> deleteAddress(String addressID) {
        HttpHeaders headers = getAuthenticatedAccess();

        HttpEntity entity = new HttpEntity(headers);
        Map<String, String> uriParams = new HashMap<>();
        uriParams.put(Constants.ID, addressID);

        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(Constants.MINIBAR.V2.ADDRESS_OPERATIONS());

        return restTemplate.exchange(builder.buildAndExpand(uriParams).toUriString(), HttpMethod.DELETE, entity, String.class);
    }

    public ResponseEntity<String> createCreditCard(CreateCreditCard createCreditCard) throws JsonProcessingException {
        HttpHeaders headers = getAuthenticatedAccess();

        CustomObjectMapper objectMapper = new CustomObjectMapper();
        String requestJson = objectMapper.writeValueAsString(createCreditCard);

        HttpEntity<String> entity = new HttpEntity<>(requestJson, headers);

        return restTemplate.exchange(Constants.MINIBAR.V2.CREATE_CREDIT_CARD(), HttpMethod.POST, entity, String.class);
    }

    public ResponseEntity<String> deleteCreditCard(String creditCardID) {
        HttpHeaders headers = getAuthenticatedAccess();

        HttpEntity entity = new HttpEntity(headers);

        Map<String, String> uriParams = new HashMap<>();
        uriParams.put(Constants.ID, creditCardID);

        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(Constants.MINIBAR.V2.CREDIT_CARD_OPERATIONS());

        return restTemplate.exchange(
                builder.buildAndExpand(uriParams).toUriString(), HttpMethod.DELETE, entity, String.class);
    }

    public ResponseEntity<String> getBraintreeSDKToken() {
        HttpHeaders headers = getAnonymousAccess();
        HttpEntity entity = new HttpEntity(headers);

        return restTemplate.exchange(Constants.MINIBAR.V2.GET_BRAINTREE_TOKEN(), HttpMethod.GET, entity, String.class);
    }

    public ResponseEntity<String> createOrder(Order order) throws JsonProcessingException {
        HttpHeaders headers = getAuthenticatedAccess();
        CustomObjectMapper objectMapper = new CustomObjectMapper();
        String requestJson = objectMapper.writeValueAsString(order);

        HttpEntity<String> entity = new HttpEntity<>(requestJson, headers);
        return restTemplate.exchange(Constants.MINIBAR.V2.CREATE_ORDER(), HttpMethod.POST, entity, String.class);
    }

    public ResponseEntity<String> finalizeOrder(String orderNumber){
        HttpHeaders headers = getAuthenticatedAccess();
        HttpEntity entity = new HttpEntity(headers);

        Map<String, String> uriParams = new HashMap<>();
        uriParams.put(Constants.ID, orderNumber);

        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(Constants.MINIBAR.V2.FINALIZE_ORDER());

        return restTemplate.exchange(builder.buildAndExpand(uriParams).toUriString(), HttpMethod.POST, entity, String.class);

    }


}
