package com.iconnect.vendor.minibar.services.database.company;

import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Service
public class EntityManagerServiceCompany {
    @PersistenceContext
    public EntityManager entityManager;
}
