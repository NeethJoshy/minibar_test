package com.iconnect.vendor.minibar.services.transformers;

import com.iconnect.http.common.CoreAddressRequest;
import com.iconnect.http.common.CoreAddressResponse;
import com.iconnect.vendor.minibar.entity.AddressDetails;

public class ToCoreAddressRequest {
    private ToCoreAddressRequest() {

    }

    public static CoreAddressRequest transform(AddressDetails addressDetail) {
        CoreAddressRequest coreAddressRequest = new CoreAddressRequest();
        coreAddressRequest.setInternalAddressID(addressDetail.getInternalAddressID());
        coreAddressRequest.setFirstName(addressDetail.getFirstName());
        coreAddressRequest.setLastName(addressDetail.getLastName());
        coreAddressRequest.setAddress1(addressDetail.getAddress1());
        coreAddressRequest.setAddress2(addressDetail.getAddress2());
        coreAddressRequest.setCompany(addressDetail.getCompany());
        coreAddressRequest.setCity(addressDetail.getCity());
        coreAddressRequest.setState(addressDetail.getState());
        coreAddressRequest.setZipcode(addressDetail.getZipcode());
        coreAddressRequest.setPhone(addressDetail.getPhone());
        return coreAddressRequest;
    }

    public static CoreAddressRequest transform(CoreAddressResponse coreAddressResponse) {
        CoreAddressRequest coreAddressRequest = new CoreAddressRequest();
        coreAddressRequest.setFirstName(coreAddressResponse.getFirstname());
        coreAddressRequest.setLastName(coreAddressResponse.getLastname());
        coreAddressRequest.setAddress1(coreAddressResponse.getAddress1());
        coreAddressRequest.setAddress2(coreAddressResponse.getAddress2());
        coreAddressRequest.setCompany(coreAddressResponse.getCompany());
        coreAddressRequest.setCity(coreAddressResponse.getCity());
        coreAddressRequest.setState(coreAddressResponse.getState());
        coreAddressRequest.setZipcode(coreAddressResponse.getZipcode());
        coreAddressRequest.setPhone(coreAddressResponse.getPhone());
        return coreAddressRequest;
    }

}
