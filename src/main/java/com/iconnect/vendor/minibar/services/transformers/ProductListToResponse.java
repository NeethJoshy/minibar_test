package com.iconnect.vendor.minibar.services.transformers;

import com.iconnect.http.response.CoreProductListResponse;
import com.iconnect.http.response.product.CorePriceResponse;
import com.iconnect.http.response.product.CoreProductResponse;
import com.iconnect.http.response.product.CoreSupplierResponse;
import com.iconnect.http.response.product.CoreVarientResponse;
import com.iconnect.vendor.minibar.core.CurrentRequestScope;
import com.iconnect.vendor.minibar.minibar.request.SuppliersList;
import com.iconnect.vendor.minibar.minibar.response.product.ProductList;
import com.iconnect.vendor.minibar.minibar.response.product.Product_grouping;
import com.iconnect.vendor.minibar.minibar.response.product.Variants;
import com.iconnect.vendor.minibar.minibar.response.supplier.ResponseSupplierList;
import com.iconnect.vendor.minibar.minibar.response.supplier.Suppliers;
import com.iconnect.vendor.minibar.model.PermalinkBrand;
import com.iconnect.vendor.minibar.model.ProductModel;
import com.iconnect.vendor.minibar.model.Products;
import org.springframework.stereotype.Service;


import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

@Service
public class ProductListToResponse {


    public CoreProductListResponse transform(ResponseSupplierList responseSupplierList, ProductList productList){

        CoreProductListResponse coreProductListResponse = new CoreProductListResponse();

        ArrayList<CoreProductResponse> coreProducts = new ArrayList<>();

        ProductModel productModel = CurrentRequestScope.getProductModel();

        Products products = productModel.getProducts();

        HashSet<String> presentSupplierID = new HashSet<>();

        for(Product_grouping product_grouping : productList.getProduct_groupings()){

            HashMap<String,CoreProductResponse> containerCoreProductResponseHashMap = new HashMap<>();

            String productPermalink = product_grouping.getPermalink().toLowerCase();

            if(products.getProductID(productPermalink) != null){

                for(Variants variants : product_grouping.getVariants()){

                    CoreVarientResponse coreVarientResponse = transformVariant(variants);

                    String containerType = variants.getContainer_type().toLowerCase();

                    CoreProductResponse coreProductResponse = containerCoreProductResponseHashMap.get(containerType);

                    if(coreProductResponse == null){

                        coreProductResponse = new CoreProductResponse();
                        containerCoreProductResponseHashMap.put(containerType,coreProductResponse);
                        PermalinkBrand permalinkBrand = products.getProductID(productPermalink);
                        coreProductResponse.setId(permalinkBrand.getProductID());
                        coreProductResponse.setBrand(products.getBrandName(permalinkBrand.getBrandKey()));
                        coreProductResponse.setContainer_type(containerType);
                        CorePriceResponse corePriceResponse = new CorePriceResponse();
                        corePriceResponse.setMax(coreVarientResponse.getPrice());
                        corePriceResponse.setMin(coreVarientResponse.getPrice());
                        coreProductResponse.setPrice_range(corePriceResponse);

                    }

                    CorePriceResponse corePriceResponse = coreProductResponse.getPrice_range();
                    if ((new BigDecimal(corePriceResponse.getMax())).compareTo(new BigDecimal(coreVarientResponse.getPrice())) == -1) {
                        corePriceResponse.setMax(coreVarientResponse.getPrice());
                    }

                    if ((new BigDecimal(corePriceResponse.getMin())).compareTo(new BigDecimal(coreVarientResponse.getPrice())) == 1) {
                        corePriceResponse.setMin(coreVarientResponse.getPrice());
                    }

                    coreProductResponse.addVariant(coreVarientResponse);

                    presentSupplierID.add(coreVarientResponse.getSupplierId());

                }
            }

            for(CoreProductResponse coreProductResponse : containerCoreProductResponseHashMap.values()){
                if(!coreProductResponse.getVariants().isEmpty()){
                    coreProducts.add(coreProductResponse);
                }
            }
        }

        ArrayList<CoreSupplierResponse> coreSuppliers = new ArrayList<>();
        for (Suppliers supplier : responseSupplierList.getSuppliers()) {

            if (!presentSupplierID.contains(String.valueOf(supplier.getId()))) {
                continue;
            }

            coreSuppliers.add(transform(supplier));
        }

        coreProductListResponse.setProducts(coreProducts);
        coreProductListResponse.setSuppliers(coreSuppliers);
        return coreProductListResponse;
    }


    public static CoreVarientResponse transformVariant(Variants variants){
        CoreVarientResponse coreVariantResponse = new CoreVarientResponse();

        coreVariantResponse.setItemId(String.valueOf(variants.getId()));
        coreVariantResponse.setPrice(String.valueOf(variants.getPrice()));
        coreVariantResponse.setQuantityOnHand(String.valueOf(variants.getIn_stock()));
        coreVariantResponse.setSupplierId(String.valueOf(variants.getSupplier_id()));
        coreVariantResponse.setVolume(variants.getShort_volume());
        if(variants.getShort_pack_size().equalsIgnoreCase("")) {
            coreVariantResponse.setPackSize("Individual");
        } else {
            coreVariantResponse.setPackSize(variants.getShort_pack_size());
        }

        return coreVariantResponse;
    }

    public static CoreSupplierResponse transform(Suppliers suppliers){
        CoreSupplierResponse coreSupplierResponse = new CoreSupplierResponse();


        coreSupplierResponse.setAddress(AddressToResponse.transform(suppliers.getAddress()));
        coreSupplierResponse.setId(String.valueOf(suppliers.getId()));
        coreSupplierResponse.setName(suppliers.getName());
        coreSupplierResponse.setType(suppliers.getType());
        coreSupplierResponse.setOpens_at(suppliers.getOpens_at());
        coreSupplierResponse.setCloses_at(suppliers.getCloses_at());
        coreSupplierResponse.setTime_zone(suppliers.getTime_zone());
        coreSupplierResponse.setDistance(suppliers.getDistance());
        coreSupplierResponse.setBest_delivery_estimate(suppliers.getBest_delivery_estimate());
        coreSupplierResponse.setBest_delivery_fee(String.valueOf(suppliers.getBest_delivery_fee()));
        coreSupplierResponse.setBest_delivery_minimum(String.valueOf(suppliers.getBest_delivery_minimum()));
        coreSupplierResponse.setDeliveryMethods(DeliveryMethodToResponse.transform(suppliers.getDelivery_methods()));

        return coreSupplierResponse;
    }
}
