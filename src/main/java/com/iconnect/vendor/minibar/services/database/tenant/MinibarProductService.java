package com.iconnect.vendor.minibar.services.database.tenant;

import com.iconnect.vendor.minibar.entity.MinibarProduct;
import com.iconnect.vendor.minibar.repo.MinibarProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;

@Service
public class MinibarProductService extends EntityManagerServiceTenant {
    @Autowired
    MinibarProductRepository minibarProductRepository;

    @Transactional
    public ArrayList<MinibarProduct> fetchALL() {
        return new ArrayList<>(minibarProductRepository.findAll());
    }

//    @Transactional
//    public MinibarProduct findMinibarProduct(MinibarProduct minibarProduct){
//        return minibarProductRepository.findByProductID(minibarProduct.getProductID());
//    }
//
//    @Transactional
//    public void deleteMinibarProduct(MinibarProduct minibarProduct){
//        minibarProductRepository.delete(minibarProduct);
//    }
//
//    @Transactional
//    public void insertMinibarProduct(MinibarProduct minibarProduct){
//        minibarProductRepository.save(minibarProduct);
//    }

}
