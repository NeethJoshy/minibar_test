package com.iconnect.vendor.minibar.services.transformers;

import com.iconnect.http.request.CoreOrderItems;
import com.iconnect.http.request.CoreOrderRequest;
import com.iconnect.vendor.minibar.minibar.request.Order;
import com.iconnect.vendor.minibar.minibar.request.Order_items;

import java.util.ArrayList;

public class RequestToOrder {
    private RequestToOrder() {

    }

    public static Order transform(CoreOrderRequest coreOrderRequest) {
        Order order = new Order();
        order.setShipping_address_id(coreOrderRequest.getShippingId());
        order.setPayment_profile_id(coreOrderRequest.getPaymentId());
        order.setTip(coreOrderRequest.getTip());
        order.setDelivery_notes(coreOrderRequest.getDeliveryNote());
        order.setOrder_items(transform(coreOrderRequest.getOrderItems()));
        order.setDelivery_method_id(coreOrderRequest.getDeliverAt());
        order.setPromo_code(coreOrderRequest.getCouponCode());
        return order;
    }
    public static ArrayList<Order_items> transform(ArrayList<CoreOrderItems> coreOrderItemsArrayList) {
        ArrayList<Order_items> order_itemsList = new ArrayList<>();
        for (CoreOrderItems coreOrderItems : coreOrderItemsArrayList) {
            Order_items order_items = new Order_items(coreOrderItems.getVendor_product_id(),Long.parseLong(coreOrderItems.getQuantity()));
            order_itemsList.add(order_items);
        }
        return order_itemsList;
    }
}
