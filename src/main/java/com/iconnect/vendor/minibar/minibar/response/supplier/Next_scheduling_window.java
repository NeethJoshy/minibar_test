package com.iconnect.vendor.minibar.minibar.response.supplier;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.io.Serializable;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Next_scheduling_window implements Serializable {
    private String end_time;

    private String start_time;
}
