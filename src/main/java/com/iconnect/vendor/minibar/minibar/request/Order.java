package com.iconnect.vendor.minibar.minibar.request;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.io.Serializable;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class Order implements Serializable {
    private String delivery_notes;

    private String promo_code;

    private String payment_profile_id;

    private String shipping_address_id;

    private List<Order_items> order_items;

    private String tip;

    private String delivery_method_id;

    public String getDelivery_notes() {
        return delivery_notes;
    }

    public void setDelivery_notes(String delivery_notes) {
        this.delivery_notes = delivery_notes;
    }

    public String getPromo_code() {
        return promo_code;
    }

    public void setPromo_code(String promo_code) {
        this.promo_code = promo_code;
    }

    public String getPayment_profile_id() {
        return payment_profile_id;
    }

    public void setPayment_profile_id(String payment_profile_id) {
        this.payment_profile_id = payment_profile_id;
    }

    public String getShipping_address_id() {
        return shipping_address_id;
    }

    public void setShipping_address_id(String shipping_address_id) {
        this.shipping_address_id = shipping_address_id;
    }

    public List<Order_items> getOrder_items() {
        return order_items;
    }

    public void setOrder_items(List<Order_items> order_items) {
        this.order_items = order_items;
    }

    public String getTip() {
        return tip;
    }

    public void setTip(String tip) {
        this.tip = tip;
    }

    public String getDelivery_method_id() {
        return delivery_method_id;
    }

    public void setDelivery_method_id(String delivery_method_id) {
        this.delivery_method_id = delivery_method_id;
    }
}
