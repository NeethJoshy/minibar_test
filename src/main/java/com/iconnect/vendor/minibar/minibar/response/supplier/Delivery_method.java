package com.iconnect.vendor.minibar.minibar.response.supplier;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.sun.source.doctree.SerialDataTree;
import lombok.Data;

import java.io.Serializable;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Delivery_method implements Serializable {

    private String delivery_minimum;

    @JsonProperty("allows_tipping")
    private String allowsTipping;

    private String supplier_id;

    private Hours hours;

    private String delivery_expectation;

    private String free_delivery_threshold;

    private String per_item_delivery_fee;

    private Next_scheduling_window next_scheduling_window;

    private String type;

    private String id;

    @JsonProperty("default")
    private String isDefault;

    @JsonIgnore
    public boolean getIsDefault() {
        return Boolean.valueOf(isDefault);
    }

    private String next_delivery;

    private String name;

    private String scheduling_mode;

    private String delivery_fee;

    @JsonProperty("allows_scheduling")
    private String allowsScheduling;

    private String maximum_delivery_expectation;
}
