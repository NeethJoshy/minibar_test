package com.iconnect.vendor.minibar.minibar.response.supplier;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.iconnect.vendor.minibar.minibar.common.Address;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Suppliers implements Serializable {
    private String closes_at;

    private Type_attribute type_attributes;

    private Double best_delivery_fee;

    private String opens_at;

    private String type;

    private Double best_delivery_minimum;

    private Supported_payment_method supported_payment_methods;

    private long id;

    private String time_zone;

    private String distance;

    private String best_delivery_estimate;

    private Address address;

    private List<String> alternatives;

    private String name;

    private Categories categories;

    private List<Delivery_method> delivery_methods;
}
