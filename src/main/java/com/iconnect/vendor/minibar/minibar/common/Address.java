package com.iconnect.vendor.minibar.minibar.common;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.io.Serializable;


@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(value= {"zip","is_default_delivery","address_id"}, allowGetters = true, allowSetters = false)
public class Address implements Serializable {

    String phone;

    @JsonProperty("zip_code")
    String zipcode;

    String name;

    String state;

    String address1;

    String address2;
    String company;

    String city;

    Long id;

    Double latitude;

    Double longitude;

    @JsonProperty("local_id")
    String localID;

    @JsonProperty("default")
    String isDefault;

    public boolean getIsDefault() {
        return Boolean.valueOf(isDefault);
    }

    @JsonProperty("zip")
    public String getZip() {
        return zipcode;
    }

    @JsonProperty("is_default_delivery")
    public boolean getIsDefaultDelivery() {
        return Boolean.valueOf(isDefault);
    }

    @JsonProperty("address_id")
    public Long getAddressID() {
        return id;
    }
}
