package com.iconnect.vendor.minibar.minibar.response;

import lombok.Data;

import java.io.Serializable;

@Data
public class AccessToken implements Serializable {
    private String created_at;

    private String expires_in;

    private String token_type;

    private String access_token;
}
