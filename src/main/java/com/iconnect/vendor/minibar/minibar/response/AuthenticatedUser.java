package com.iconnect.vendor.minibar.minibar.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.iconnect.vendor.minibar.minibar.common.Address;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class AuthenticatedUser implements Serializable {
    private String id;

    private String first_name;

    private String referral_code;

    private String user_token;

    private String email;

    private String last_name;

    private String test_group;

    private List<Address> shipping_addresses;

    private List<CreditCard> payment_profiles;

    private String hashed_email;

    private String order_count;
}
