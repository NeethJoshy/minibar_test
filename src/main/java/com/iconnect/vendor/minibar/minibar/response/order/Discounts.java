package com.iconnect.vendor.minibar.minibar.response.order;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.io.Serializable;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Discounts implements Serializable {
    private Double coupons;

    private Double deals;
}
