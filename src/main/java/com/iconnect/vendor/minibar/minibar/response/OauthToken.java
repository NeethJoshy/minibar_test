package com.iconnect.vendor.minibar.minibar.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class OauthToken {
    private String created_at;
    private long expires_in;
    private String refresh_token;
    private boolean resource_owner;
    private String token_type;
    private String access_token;
}
