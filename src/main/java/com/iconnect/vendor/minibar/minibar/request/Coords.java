package com.iconnect.vendor.minibar.minibar.request;

import lombok.Data;

@Data
public class Coords {
    String latitude;
    String longitude;
}
