package com.iconnect.vendor.minibar.minibar.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.iconnect.vendor.minibar.minibar.common.Address;
import lombok.Data;

import java.io.Serializable;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(value={ "last_4_digits", "card_type" , "is_default" }, allowGetters=true)
public class CreditCard implements Serializable {
    private Long id;

    private String cc_exp_month;

    private String cc_exp_year;

    private String cc_card_type;

    public String getCard_type() {
        return cc_card_type;
    }

    private String cc_last_four;

    public String getLast_4_digits() {
        return cc_last_four;
    }

    @JsonProperty("default")
    private String is_default;

    @JsonProperty("is_default")
    public boolean getis_leagacy_default() {
        return Boolean.valueOf(is_default);
    }

    public boolean getIs_default() {
        return Boolean.valueOf(is_default);
    }

    private Address address;
}
