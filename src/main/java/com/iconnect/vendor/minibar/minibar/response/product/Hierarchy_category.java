package com.iconnect.vendor.minibar.minibar.response.product;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.io.Serializable;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Hierarchy_category implements Serializable {
  private  String permalink;
  private  String name;
}
