package com.iconnect.vendor.minibar.minibar.response.order;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.iconnect.vendor.minibar.minibar.common.Address;
import com.iconnect.vendor.minibar.minibar.response.CreditCard;
import lombok.Data;

import java.io.Serializable;
import java.util.ArrayList;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ResponseOrder implements Serializable {
    Address shipping_address;

    String delivery_notes;

    Amounts amounts;

    String display_state;

    CreditCard payment_profile;

    String promo_code;

    String state;

    ArrayList<ResponseOrderItems> order_items;

    ArrayList<String> qualified_deals;

    String number;
}
