package com.iconnect.vendor.minibar.minibar.request;

public class Order_items {

    public Order_items(String id, Long quantity) {
        this.id = id;
        this.quantity = quantity;
    }

    private String id;

    private Long quantity;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Long getQuantity() {
        return quantity;
    }

    public void setQuantity(Long quantity) {
        this.quantity = quantity;
    }
}
