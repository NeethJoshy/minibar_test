package com.iconnect.vendor.minibar.minibar.response.order;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.io.Serializable;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ResponseOrderItems implements Serializable {
    private String total;

    private Long id;

    private Long supplier_id;

    private String price;

    private String product_id;

    private String name;

    private String volume;

    private Long quantity;

    private String scheduled_for;

    private Long delivery_method_id;
}
