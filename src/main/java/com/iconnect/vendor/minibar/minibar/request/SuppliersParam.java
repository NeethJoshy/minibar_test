package com.iconnect.vendor.minibar.minibar.request;

import lombok.Data;

import java.util.List;

@Data
public class SuppliersParam {
    String zipcode;
    Coords coords;
    List<String> product_grouping_ids;
}
