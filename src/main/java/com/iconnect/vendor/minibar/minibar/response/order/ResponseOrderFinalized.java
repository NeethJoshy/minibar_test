package com.iconnect.vendor.minibar.minibar.response.order;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.iconnect.rowjson.RawObject;
import com.iconnect.vendor.minibar.minibar.response.supplier.Suppliers;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ResponseOrderFinalized extends ResponseOrder implements Serializable {
    private List<String> shipping_method_types;
    private String completed_at;
    private List<Suppliers> suppliers;

    private RawObject status_actions;


    private RawObject tracking;

}
