package com.iconnect.vendor.minibar.minibar.request;

import com.iconnect.vendor.minibar.minibar.common.Address;
import lombok.Data;

import java.io.Serializable;

@Data
public class CreateCreditCard implements Serializable {
    Address address;
    String payment_method_nonce;
}
