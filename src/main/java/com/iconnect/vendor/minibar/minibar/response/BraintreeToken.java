package com.iconnect.vendor.minibar.minibar.response;

import lombok.Data;

import java.io.Serializable;
@Data
public class BraintreeToken implements Serializable {
    String encryption_key;
    String client_token;
}
