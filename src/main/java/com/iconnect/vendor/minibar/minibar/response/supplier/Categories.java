package com.iconnect.vendor.minibar.minibar.response.supplier;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.io.Serializable;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Categories implements Serializable {
    private Integer wine;

    private Integer mixers;

    private Integer other;

    private Integer beer;

    private Integer liquor;
}
