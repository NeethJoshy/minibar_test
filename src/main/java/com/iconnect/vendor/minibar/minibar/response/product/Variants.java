package com.iconnect.vendor.minibar.minibar.response.product;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.iconnect.rowjson.RawObject;
import lombok.Data;

import java.io.Serializable;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Variants implements Serializable {
    private long supplier_id;
    private long in_stock;
    private double original_price;

    private String subgroup_id;

    private String thumb_url;

    private RawObject deals;

    private long id;

    @JsonProperty("item_id")
    public long getItem_id() {
        return id;
    }

    private double price;

    private String short_pack_size;

    private String permalink;

    private String image_url;

    private String volume;

    private String short_volume;

    private String container_type;


    private Long store_id;
    private String store_name;
    private Double delivery_minimum;
    private Double delivery_fee;
    private RawObject two_for_one;

}
