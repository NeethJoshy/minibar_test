package com.iconnect.vendor.minibar.minibar.response.product;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.iconnect.rowjson.RawObject;
import lombok.Data;

import java.io.Serializable;
import java.util.List;


@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ProductList implements Serializable {
    private String count;

    private List<String> facets;

    private List<String> promotions;

    private List<Product_grouping> product_groupings;

    private RawObject search_id;
}
