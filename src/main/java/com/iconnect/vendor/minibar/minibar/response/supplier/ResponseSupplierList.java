package com.iconnect.vendor.minibar.minibar.response.supplier;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ResponseSupplierList implements Serializable {

    private String deferrable_present;

    private List<Suppliers> suppliers = new ArrayList<>();
}
