package com.iconnect.vendor.minibar.minibar.response.product;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.io.Serializable;
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ExternalProducts implements Serializable {
    private String id;
    private String permalink;
    private String min_price;
    private String max_price;
    @JsonProperty("container_type")
    private String containerType;
    private String volume;
    private String short_pack_size;
    private String short_volume;
    private String thumb_url;
    private String image_url;


}
