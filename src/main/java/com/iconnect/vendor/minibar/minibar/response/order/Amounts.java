package com.iconnect.vendor.minibar.minibar.response.order;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.io.Serializable;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Amounts implements Serializable {
    private Double total;

    private Double shipping;

    private Double tax;

    private Discounts discounts;

    private Double subtotal;

    private Double coupon;

    private Double tip;
}
