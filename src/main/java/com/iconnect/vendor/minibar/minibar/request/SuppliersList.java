package com.iconnect.vendor.minibar.minibar.request;

import lombok.Data;

import java.util.List;

@Data
public class SuppliersList {
    List<String> suppliersLists;
    String brands;

    public boolean hasBrands() {
        return brands != null;
    }
}
