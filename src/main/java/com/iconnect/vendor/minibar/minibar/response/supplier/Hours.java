package com.iconnect.vendor.minibar.minibar.response.supplier;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.io.Serializable;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Hours implements Serializable {
    private String always_open;

    private String closes_at;

    private String opens_at;
}
