package com.iconnect.vendor.minibar.minibar.response;

import lombok.Data;

import java.io.Serializable;

@Data
public class SuccessResponse implements Serializable {
    boolean success;
}
