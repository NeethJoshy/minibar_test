package com.iconnect.vendor.minibar.minibar.response.supplier;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.io.Serializable;


@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Supported_payment_method implements Serializable {
    private boolean credit_card;

    private boolean android_pay;

    private boolean apple_pay;

}
