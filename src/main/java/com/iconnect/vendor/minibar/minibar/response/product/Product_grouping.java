package com.iconnect.vendor.minibar.minibar.response.product;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.iconnect.rowjson.RawObject;
import lombok.Data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Product_grouping  implements Serializable {
    private List<String> tags;

    private String supplier_id;

    private Hierarchy_type hierarchy_type;

    private String product_name;

    private String thumb_url;

    private List<Properties> properties;

    private String type;

    private List<String> deals;

    private String id;

    private String category;

    private String permalink;

    private String product_content;

    private String image_url;

    private String description;

    private String name;

    private List<Variants> variants;

    private String brand;

    private Hierarchy_category hierarchy_category;

    private Hierarchy_subtype hierarchy_subtype;

    private RawObject brand_data;

    private ArrayList<ExternalProducts> external_products;

    private RawObject browse_type;
}
