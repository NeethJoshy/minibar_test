package com.iconnect.vendor.minibar.controller;
import com.iconnect.http.common.CoreAddressRequest;
import com.iconnect.http.common.CoreAddressResponse;
import com.iconnect.http.request.CoreAddressRequestProductList;
import com.iconnect.http.request.CoreBillingRequest;
import com.iconnect.http.request.CoreDeliverAtRequest;
import com.iconnect.http.request.CoreOrderRequest;
import com.iconnect.http.response.*;
import com.iconnect.http.response.order.CoreOrderResponseContainer;
import com.iconnect.http.response.product.CorePriceListResponse;
import com.iconnect.vendor.minibar.Configurations.Constants;

import com.iconnect.vendor.minibar.services.business.MinibarCoreLogic;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.io.IOException;


@RestController
@RequestMapping(value = {Constants.SERVICE_V2_URL})
public class MinibarController {

    @Autowired
    MinibarCoreLogic minibarCoreLogic;

    @RequestMapping(value = "/products",produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public CoreProductListResponse products(@ModelAttribute CoreAddressRequestProductList address) throws IOException {
        return minibarCoreLogic.getProducts(address);
    }

    @RequestMapping(value = "/pricerange/{id}",produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public CorePriceListResponse priceRange(@PathVariable String id) throws IOException{
        return minibarCoreLogic.getProducts(id);
    }

    @RequestMapping(value = "/address")
    @ResponseBody
    public CoreAddressListResponse getAddress() throws IOException{
        return minibarCoreLogic.getAddress();
    }


    @RequestMapping(value = "/address",method = RequestMethod.POST)
    @ResponseBody
    @ResponseStatus(HttpStatus.CREATED)
    public CoreAddressResponse createAddress(@Valid @RequestBody CoreAddressRequest coreAddressRequest) throws IOException{
        return minibarCoreLogic.createAddress(coreAddressRequest);
    }

    @RequestMapping(value = "/address/{addressId}",method = RequestMethod.DELETE)
    @ResponseBody
    public String deleteAddress(@PathVariable String addressId) throws IOException{
        return minibarCoreLogic.deleteAddress(addressId);
    }

    @RequestMapping(value = "/billing")
    public CoreBillingListResponse getBillingInfo() throws IOException {
        return minibarCoreLogic.getBillingInfo();
    }

    @RequestMapping(value = "/billing", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    public CoreBillingResponse createBillingInfo(@Valid @RequestBody CoreBillingRequest billingRequest) throws IOException {
        return minibarCoreLogic.createBillingInfo(billingRequest);
    }

    @RequestMapping(value = "/billing/{billingID}", method = RequestMethod.DELETE)
    @ResponseBody
    public String deleteBilling(@PathVariable String billingID) throws IOException {
        return minibarCoreLogic.deleteBillingInfo(billingID);
    }

    @RequestMapping(value = "/billingtoken")
    public CoreBillingTokenResponse getBillingToken() throws IOException {
        return minibarCoreLogic.fetchBillingToken();
    }

    @RequestMapping(value = "/order", method = RequestMethod.POST)
    public CoreOrderResponseContainer createOrder(@Valid @RequestBody CoreOrderRequest coreOrderRequest) throws IOException {
        return minibarCoreLogic.createOrder(coreOrderRequest);
    }

    @RequestMapping(value = "finalize/{orderNumber}",method = RequestMethod.POST)
    public CoreOrderResponseContainer finalizeOrder(@PathVariable String orderNumber,@RequestBody (required = false) CoreDeliverAtRequest coreDeliverAtRequest) throws IOException{
        return minibarCoreLogic.finalizeOrder(orderNumber,coreDeliverAtRequest);
    }
}
