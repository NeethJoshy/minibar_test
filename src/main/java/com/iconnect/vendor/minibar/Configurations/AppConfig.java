package com.iconnect.vendor.minibar.Configurations;


import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

@Component
@Configuration
@ConfigurationProperties(prefix = "app.config")
@Data
public class AppConfig {
    private String minibarURL;
}
