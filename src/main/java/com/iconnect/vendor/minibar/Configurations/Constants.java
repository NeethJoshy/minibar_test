package com.iconnect.vendor.minibar.Configurations;

import org.springframework.stereotype.Component;

@Component
public class Constants {
    public static final String SERVICE_V2_URL = "/api/v2/";
    public static final String TABLE_COLUMN_TENANT ="tenant_id" ;
    public static final String TABLE_COLUMN_IDENTIFIER = "identifier_id";
    static AppConfig appConfig;
    public static final String VENDOR = "Minibar";


    public static final String HTTP_IDENTIFIER_HEADER = "x-identifierid";
    public static final String HTTP_TENANT_HEADER = "x-tenantid";
    public static final String HTTP_CUSTOMER_HEADER = "x-customerid";
    public static final String ADDRESS_NAMESPACE = "ADDRESS-";
    public static final String CUSTOMER_MICROSERVICE = "http://CUSTOMER-MICROSERVICE";
    public static final String INTERNAL = CUSTOMER_MICROSERVICE + "/internal";
    public static final String CUSTOMER_MICROSERVICE_ADDRESS = INTERNAL + "/api/v2/address";
    public static final String ID = "id";
    public static class MINIBAR {

        private MINIBAR() {

        }

        public static final String ACCESS_TOKEN() {
            return "https://sandbox.minibardelivery.com/oauth/token";
            //return appConfig.getMinibarURL() + "/oauth/token";
        }

        public static final String CLIENT_ID_PARAM = "client_id";
        public static final String CLIENT_SECRET_PARAM = "client_secret";
        public static final String GRANT_TYPE_PARAM = "grant_type";
        public static final String GRANT_TYPE_VALUE = "client_credentials";



        public static final String COORDS_LATITUDE = "coords[lat]";
        public static final String COORDS_LONGITUDE = "coords[lng]";
        public static final String ROUTING_OPTIONS_PRODUCT_GROUPING = "routing_options[product_grouping_ids][]";

        public static final String GET_PRODUCTS_PLACEHOLDER = "supplierslist";
        public static final String MINIBAR_GLOBAL_QUERY = "brand";


        public static class V2 {
            private V2() {

            }
            public static String GET_SUPPLIERS() {
                return "https://sandbox.minibardelivery.com/api/v2/suppliers";
            }

            public static String GET_PRODUCTS() {
                return "https://sandbox.minibardelivery.com/api/v2/supplier/{" + GET_PRODUCTS_PLACEHOLDER + "}/product_groupings";
            }

            public static String GET_PRODUCTS_PRICE_RANGE(String ID) {
                return "https://sandbox.minibardelivery.com/api/v2/product_grouping/"+ID;
            }

            public static String AUTHENTICATE_OAUTH() {
                return "https://sandbox.minibardelivery.com/api/v2/auth/token";
            }

            public static String CREATE_USER() {
                return "https://sandbox.minibardelivery.com/api/v2/user";
            }

            public static String CREATE_ADDRESS() {
                return  "https://sandbox.minibardelivery.com/api/v2/user/shipping";
            }

            public static String ADDRESS_OPERATIONS() {
                return CREATE_ADDRESS() + "/{" + ID + "}";
            }

            public static String CREATE_CREDIT_CARD() {
                return "https://sandbox.minibardelivery.com/api/v2/user/payment_profile";
            }

            public static String CREDIT_CARD_OPERATIONS() {
                return CREATE_CREDIT_CARD() + "/{" + ID + "}";
            }

            public static String GET_BRAINTREE_TOKEN() {
                return  "https://sandbox.minibardelivery.com/api/v2/client";
            }


            public static String CREATE_ORDER() {
                return "https://sandbox.minibardelivery.com/api/v2/order";
            }

            public static String FINALIZE_ORDER() {
                return CREATE_ORDER() + "/{" + ID + "}/actions/finalize";
            }

        }

    }
}
