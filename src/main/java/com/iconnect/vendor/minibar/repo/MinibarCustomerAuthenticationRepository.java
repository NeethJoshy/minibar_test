package com.iconnect.vendor.minibar.repo;

import com.iconnect.vendor.minibar.entity.MinibarCustomerAuthentication;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface MinibarCustomerAuthenticationRepository extends JpaRepository<MinibarCustomerAuthentication, Long> {

    MinibarCustomerAuthentication findByCustomerid(String customerid);
}
