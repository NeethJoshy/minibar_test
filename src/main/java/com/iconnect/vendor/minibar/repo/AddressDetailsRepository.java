package com.iconnect.vendor.minibar.repo;

import com.iconnect.vendor.minibar.entity.AddressDetails;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


import java.util.List;

@Repository
public interface AddressDetailsRepository extends JpaRepository<AddressDetails,Long> {


    List<AddressDetails> findAllByCustomerID(String customerID);
    AddressDetails findByVendorAddressIDAndCustomerID(String vendorAddressID,String customerID);
    AddressDetails findByInternalAddressIDAndCustomerID(String internalAddressID,String customerID);
}
