package com.iconnect.vendor.minibar.repo;

import com.iconnect.vendor.minibar.entity.MinibarOrder;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MinibarOrderRepo extends JpaRepository<MinibarOrder, Long> {
    MinibarOrder findByCustomerIDAndVendorOrderNumber(String customerID,String orderNumber);
}
