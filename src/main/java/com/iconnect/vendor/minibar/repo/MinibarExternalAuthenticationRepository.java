package com.iconnect.vendor.minibar.repo;

import com.iconnect.vendor.minibar.entity.MinibarExternalAuthentication;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MinibarExternalAuthenticationRepository extends JpaRepository<MinibarExternalAuthentication, Long> {
}
