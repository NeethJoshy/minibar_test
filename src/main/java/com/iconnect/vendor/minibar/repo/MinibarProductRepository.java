package com.iconnect.vendor.minibar.repo;

import com.iconnect.vendor.minibar.entity.MinibarProduct;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MinibarProductRepository extends JpaRepository<MinibarProduct, Long> {
    public MinibarProduct findByProductID(String productID) ;
}
